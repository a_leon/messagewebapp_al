package pl.sda.springmvc.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.springmvc.model.*;
import pl.sda.springmvc.service.*;



//TODO sprawdzic authoryzacje na roznych stronach (wpisanie z palca)
@Controller
@RequestMapping("/")
public class AppController {

	@Autowired
	UserService userService;

	@Autowired
	UserProfileService userProfileService;

	@Autowired
	MessageService messageService;

	@Autowired
	ItemService itemService;

	@Autowired
	MessageSource messageSource;

	@Autowired
	MailService mailService;

	@Autowired
	UserNotificationService userNotificationService;

	@Autowired
	UserStateService userStateService;

	/*
 	* This method redirect to default view.
 	*/
	@RequestMapping(value = { "/"}, method = RequestMethod.GET)
	public String goToLoginPage(ModelMap model) {
		return "redirect:/login";
	}

	/*
	 * This method will list all existing users. In general - available only to admin
	 */
	//TODO - checked
	@RequestMapping(value = {"/list" }, method = RequestMethod.GET)
	public String listUsers(ModelMap model) {
		model.addAttribute("users", userService.findAllUsers());
		return "allusers";
	}

	/*
	 * This method will provide the medium to add a new users.
	 */
	//TODO - checked
	@RequestMapping(value = { "/newUser" }, method = RequestMethod.GET)
	public String newUserRegistration(HttpServletRequest request, ModelMap model) {
		model.addAttribute("user", new User());
//		model.addAttribute("edit", false);
		addUrlPriorToVisitToSession(request);
		return "newUser";
	}


	/*
 	* This method will be called on form submission, handling POST request It
    * also validates the user input.
    */
	//TODO - checked
	//TODO - add js password validation
	@RequestMapping(value = { "/newUser" }, method = RequestMethod.POST)
	public String saveRegisteredUser(@Valid User user, BindingResult result,
									 ModelMap model, HttpServletRequest request,
									 RedirectAttributes redirectAttrs) {
		String redirectUrl = getUrlPriorToVisitFromSession(request);

		if(!user.getPassword().equals(user.getConfirmedPassword())){
			System.out.println("There are errors in your input");
			model.addAttribute("success", "Passwords do not match");
			return "newUser";
		}

		if (result.hasErrors()) {
			System.out.println("There are errors in your input");
			return "newUser";
		}

		// checks whether user with given login or email is in database
		// to prevent hibernate unique constraint exception
		if(userService.findByLoginOrEmail(user.getLogin(), user.getEmail()).isPresent()){
			model.addAttribute("success", "User with given login and/or email address" +
					" already exists. Please set different login / email.");
			return "newUser";
		}

		// if account is not created by admin, form has no userProfiles to choose
		// therefore the account has default 'USER' profile added
		userService.assignUserProfileToUserIfEmpty(user);
		userService.setUsersUniqueEmailConfirmationId(user);
		userService.saveUser(user);
		userNotificationService.sendRegistrationConfirmation(user);

		redirectAttrs.addFlashAttribute("success", "User " + user.getLogin()
				+ " has been registered successfully. Confirmation"
				+ " email has been sent to user's email address.");

		return "redirect:" + redirectUrl;
	}


	/*
 	* This method will add the previously visited URL taken from session to
 	* be passed to addUrlPriorToVisitToSession() redirect
 	* to page visited previously to e.g. adding new user
    */
	private void addUrlPriorToVisitToSession(HttpServletRequest request){
		String referrer = request.getHeader("Referer");
		request.getSession().setAttribute("url_path_prior", shortenUrlToPathOnly(referrer));
	}

	/*
 	* This method will return the redirect URL from session to redirect
 	* to page visited previously to e.g. adding new user
    */
	private String getUrlPriorToVisitFromSession(HttpServletRequest request){
		HttpSession session = request.getSession();
		return (String) session.getAttribute("url_path_prior");
	}

	/*
 	* This method will be called to cut the url to return only path
    */
	public String shortenUrlToPathOnly(String redirectUrl){
		String[] referrerTrimmedPageArray = redirectUrl.split("8080");
		return referrerTrimmedPageArray[1];
	}

	/*
 	* This method will be called to handle forgot password request, handling POST request It
    */
	@RequestMapping(value = { "/forgot" }, method = RequestMethod.GET)
	public String forgotPasswordForm(ModelMap model) {
		return "forgotPassword";
	}

	/*
 	* This method will be called to handle forgot password request, handling POST request It
	*/
	@RequestMapping(value = { "/forgot" }, method = RequestMethod.POST)
	public String forgotPasswordFormSubmission(@RequestParam String email, ModelMap model) {
		String view = "forgotPassword";
		Optional<User> user = userService.findUserByEmail(email);

		if(user.isPresent()){
			//TODO future - add confirmationId expiration time
			userService.setUsersUniqueEmailConfirmationId(user.get());
			userService.updateUser(user.get());
			userNotificationService.sendForgottenPasswordEmail(user.get());
			model.addAttribute("success", "New message with link password changing page was sent to: " + user.get().getEmail()
					+ ".\nPlease check your mail.");
			view = "forgotPasswordSent";
		} else {
			model.addAttribute("success", "There is no user with email: " + email+ " registered. Please try again.");
		}
		return view;
	}

	/*
 	* This method will be called to handle forgot password request, handling POST request It
	*/
	//TODO - checked
	@RequestMapping(value = { "/forgot/check/{confirmId}" }, method = RequestMethod.GET)
	public String forgotPasswordCheckAndChange(ModelMap model, @PathVariable String confirmId) {
		String view = "forgotPassword";
		Optional<User> user = userService.findUserByEmailConfirmationId(confirmId);
		if(user.isPresent()){
			if(user.get().getState().equals("Inactive")){
				user.get().setState(UserState.ACTIVE.toString());
			}
			userService.updateUser(user.get());
			view = "changeForgottenPassword";
		} else{
			model.addAttribute("success", "Wrong password changing code. Please try again.");
		}
		return view;
	}

	/*
 	* This method will be called to handle forgot password request, handling POST request It
	*/
	//TODO - checked
	@RequestMapping(value = { "/forgot/check/{confirmId}" }, method = RequestMethod.POST)
	public String changeForgottenPassword(ModelMap model, @PathVariable String confirmId, @RequestParam String password,
										  @RequestParam String login) {
		String successMessage ="";
		Optional<User> user = userService.findUserByEmailConfirmationId(confirmId);
		if(user.isPresent() && user.get().getLogin().equals(login)){
			userService.setNewEncryptedPassword(user.get(), password);
			user.get().setEmailConfirmationId(null);
			userService.updateUserConfirmationId(user.get());
			model.addAttribute("newPasswordSuccess", "true");
			successMessage = "Your password has been successfully changed. " +
					"Please try to login with new credentials.";
		} else {
			successMessage = "Password has not been changed. Please try again.";
		}

		model.addAttribute("success", successMessage);
		return "forgotPasswordSent";
	}

	//TODO - checked
	@RequestMapping(value = { "/confirm/{confirmId}" }, method = RequestMethod.GET)
	public String emailConfirmation(@PathVariable String confirmId, ModelMap model) {
		Optional<User> user = userService.findUserByEmailConfirmationId(confirmId);
		if(user.isPresent()){
			user.get().setState(UserState.ACTIVE.toString());
			user.get().setEmailConfirmationId(null);
			userService.updateUser(user.get());
			model.addAttribute("success", "Your account is activated. Please login.");
		}
		return "login";
	}

	/*
	 * This method will provide the medium to update an existing user.
	 */
	//TODO-checked
	@RequestMapping(value = { "/edit-{id}-user" }, method = RequestMethod.GET)
	public String editUser(@PathVariable String id, ModelMap model, HttpServletRequest request) {
		Optional<User> user = userService.findById(Integer.parseInt(id));
		addUrlPriorToVisitToSession(request);
		if (user.isPresent()){
			userService.simpleAuthenticationWithAdminOrUser(user.get());
			model.addAttribute("user", user.get());
			model.addAttribute("edit", true);
		}
		return "newUser";
	}
	
	/*
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 */
	//TODO - future - threat - possible edition by user after deleting from database by admin (in the "meantime")
	//TODO - checked
	@RequestMapping(value = { "/edit-{id}-user" }, method = RequestMethod.POST)
	public String updateUser(@Valid User user, BindingResult result,
			ModelMap model, @PathVariable String id, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {

		Optional<User> userByID = userService.findById(Integer.parseInt(id));
		String redirectUrl = getUrlPriorToVisitFromSession(request);
		userService.simpleAuthenticationWithAdminOrUser(userByID.get());

		if (result.hasErrors()) {
			model.addAttribute("success", "There are errors in edited user. Please try again");
			model.addAttribute("user", user);
			return editUser(id, model, request);
		}

		/* Method checks whether login (if) changed is unique
		 */
		if(!userByID.get().getLogin().equals(user.getLogin())){
			if(userService.findByLogin(user.getLogin()).isPresent()){
				model.addAttribute("user", userByID.get());
				model.addAttribute("success", "User with given login" +
						" already exists. Please set different login.");
				return editUser(id, model, request);
			}
		}

		/* Method checks whether email (if) changed is unique and can be modified
		* If true - sends email to old and new email address with appropriate information
		* and sets User State as Inactive - new mail has to be confirmed
		 */
		if(!userByID.get().getEmail().equals(user.getEmail())){
			if(userService.findUserByEmail(user.getEmail()).isPresent()){
				model.addAttribute("user", userByID.get());
				model.addAttribute("success", "User with given email address" +
						" already exists. Please set different email.");
				return editUser(id, model, request);
			} else {
				user.setState("Inactive");
				userService.setUsersUniqueEmailConfirmationId(user);
				userNotificationService.sendChangedMailAddressEmail(userByID.get(), user);
			}
		}
		userService.updateUser(user);
		redirectAttributes.addFlashAttribute("success", "User " + user.getFirstName() + " " + user.getLastName() + " updated successfully");
		return "redirect:" + redirectUrl;
	}

	/*
 	* This method will provide the medium to update an existing item.
 	*/
	//TODO - checked
	@RequestMapping(value = { "/user-{id}/item-{item_id}/edit" }, method = RequestMethod.GET)
	public String editItem(@PathVariable String id, @PathVariable String item_id, ModelMap model, HttpServletRequest request) {
		Optional<User> user = userService.findById(Integer.parseInt(id));
		Optional<Item> itemToBeEdited = itemService.findById(Integer.parseInt(item_id));
		addUrlPriorToVisitToSession(request);

		if (user.isPresent() && itemToBeEdited.isPresent()){
			userService.simpleAuthentication(user.get());
			addItemFormValuesToModelForEditing(model, itemToBeEdited.get());
			Item item = itemToBeEdited.get();
			//TODO change to comparing old item to new (updateItem.POST) in terms of assignedToId
			if (item.getAssignedTo()!=null){
				Integer currentAssignedToId = item.getAssignedTo().getId();
				item.setAssignedToId(currentAssignedToId);
				request.getSession().setAttribute("assignedToIdPriorToChange", currentAssignedToId);
			}
			model.put("item", item);
			model.addAttribute("user", user.get());
			model.addAttribute("edit", true);
		} else {
			model.addAttribute("success", "There is no user / item with given id");
		}
		return "editItem";
	}

	/*
 	* This method will be called on form submission, handling POST request for
 	* updating item in database. It also validates the item input
 	*/
	//TODO - checked
	@RequestMapping(value = { "/user-{id}/item-{item_id}/edit" }, method = RequestMethod.POST)
	public String updateItem(@Valid Item item, BindingResult result, @PathVariable String id,
							 ModelMap model, HttpServletRequest request, RedirectAttributes redirectAttrs) {

		HttpSession session = request.getSession();
		String redirectUrl = getUrlPriorToVisitFromSession(request);

		Optional<Integer> assignedToIdPriorToChange =
				Optional.ofNullable((Integer) session.getAttribute("assignedToIdPriorToChange"));
		Optional<User> newUserAssigned = itemService.getAndSetUserAssignedFromAssignedId(item);

		if (result.hasErrors()) {
			model.addAttribute("success", "There are errors in edited item.");
			return editItem(id, String.valueOf(item.getItemId()),model, request);
		}

		/*
		checks whether assignedToId is !=0 and new assigned user was not found
		 which indicates that assigned user does not exists in database
		*/
		if (item.getAssignedToId()!=0 && !newUserAssigned.isPresent()){
			model.addAttribute("success", "There is no user with given id to assign item to");
			return editItem(id, String.valueOf(item.getItemId()),model, request);
		} else {
			/*checks whether new user is assigned to item
			* if so - depending on whether item already had user assigned
			* either send mail to new user (if no user was previously assigned to item)
			* or checks if assigned user has changed and send notification if true
			*/
			if(newUserAssigned.isPresent()){
				if(assignedToIdPriorToChange.isPresent()){
					userNotificationService.sendMailToUserIfAssignedChanged(newUserAssigned.get(),
							assignedToIdPriorToChange, item);
				} else {
					userNotificationService.sendEmailToUserAssignedToItem(newUserAssigned.get(), item);
				}
			}
			item.updateTotalSpentTime(item.getTimeSpentSinceLastEdit());
			itemService.updateItem(item);
			redirectAttrs.addFlashAttribute("success", "Item " + item.getItemId() + " " + item.getTitle() + " was successfully updated ");
		}
		return "redirect:" + redirectUrl;
	}

	//TODO-checked
	//TODO - add view message
	@RequestMapping(value = { "/user-{id}/item-{item_id}" }, method = RequestMethod.GET)
	public String viewItem(@PathVariable String id, @PathVariable String item_id, ModelMap model) {
		Optional<User> user = userService.findById(Integer.parseInt(id));
		Optional<Item> item = itemService.findById(Integer.parseInt(item_id));

		if (user.isPresent() && item.isPresent()){
			userService.simpleAuthentication(user.get());
			model.put("item", item.get());
			model.addAttribute("user", user.get());
			if(checkUserPrivilegeToEditOrDeleteItem(user.get(), item.get())){
				model.addAttribute("userAuthorized", true);
			}
		}
		return "itemView";
	}

	/*
 	* This method will be called by clicking link to "Delete" message,
 	* deleting message in database. Firstly it checks user's authorization
 	* to delete message and redirect's to the page from which the message was deleted.
 	*/
	//TODO - checked
	@RequestMapping(value = { "/user-{id}/msg-{msg_id}/delete" }, method = RequestMethod.GET)
	public String deleteMessage(@PathVariable String id, @PathVariable String msg_id, HttpServletRequest request,
								RedirectAttributes redirectAttrs) {
		addUrlPriorToVisitToSession(request);
		String redirectUrl = getUrlPriorToVisitFromSession(request);
		Optional<User> user = userService.findById(Integer.parseInt(id));
		Optional<Message> message = messageService.findById(Integer.parseInt(msg_id));

		if (user.isPresent() && message.isPresent()) {
			userService.simpleAuthentication(user.get());
			messageService.deleteMessageById(Integer.parseInt(msg_id));
			redirectAttrs.addFlashAttribute("success", "Message " + msg_id + " " + " deleted successfully");
			redirectAttrs.addFlashAttribute("user", user.get());
		} else {
			redirectAttrs.addFlashAttribute("success", "There is no user or message with " +
					"given id in database.");
		}

		return "redirect:" + redirectUrl;
	}

	/*
 	* This method will be called clicking link to "Delete" item,
 	* deleting item in database. Firstly it checks user's authorization
 	* to delete item, adds proper message depending on the result (deleted / not deleted)
 	* and redirect's to the page from which the item was deleted.
 	*/
	@RequestMapping(value = { "/user-{id}/item-{item_id}/delete" }, method = RequestMethod.GET)
	public String deleteItem(@PathVariable String item_id, @PathVariable String id, ModelMap model,
							 HttpServletRequest request, RedirectAttributes redirectAttrs) {
		Optional<User> user = userService.findById(Integer.parseInt(id));
		Optional<Item> item = itemService.findById(Integer.parseInt(item_id));
		addUrlPriorToVisitToSession(request);
		String redirectUrl = getUrlPriorToVisitFromSession(request);

		if (user.isPresent() && item.isPresent()) {
			if(checkUserPrivilegeToEditOrDeleteItem(user.get(), item.get())){
				itemService.deleteItemById(Integer.parseInt(item_id));
				redirectAttrs.addFlashAttribute("success", "Item titled: " + item.get().getTitle()
						+ " deleted successfully!");
				redirectAttrs.addFlashAttribute("user", user.get());
			}
		} else {
			redirectAttrs.addFlashAttribute("success", "There is no user or item with " +
					"given id in database.");
		}
		return "redirect:" + redirectUrl;
	}


	public boolean checkUserPrivilegeToEditOrDeleteItem(User user, Item item){
		boolean privilege=false;
		int userId = user.getId();

		if (item.getAssignedTo()!=null){
			if(item.getAssignedTo().getId()==userId || item.getCreatedBy().getId()==userId){
				privilege=true;
			}
		}
		return privilege;
	}

	/*
	 * This method will delete an user by it's id value.
	 */
	/*TODO - future - add sending mail to createdby user with info that user
	 being assigned to item is deleted*/
	@RequestMapping(value = { "/delete-{id}-user" }, method = RequestMethod.GET)
	public String deleteUser(@PathVariable String id) {
		userService.simpleAuthenticationWithIdStringOnly(id);
		userService.deleteUserById(Integer.parseInt(id));
		return "redirect:/list";
	}

	/*
	 * This method will redirect to find user form - available to admin only.
	 */
	@RequestMapping(value = { "/find" }, method = RequestMethod.GET)
	public String findForm(ModelMap map) {
		map.addAttribute("user", new User());
		map.addAttribute("username", getPrincipal());
		return "findUser";
	}

	/*
	 * This method will list users found by any value (id, name, surname)
	 * given in the find user form.
	 */
	@RequestMapping(value = { "/find" }, method = RequestMethod.POST)
	public String showFindResult(ModelMap model, @ModelAttribute User user) {
		model.addAttribute("searchList", "True");
		if((Integer) user.getId()==null){
			model.addAttribute("success", "The id field should be set as 0 if not known");
			return findForm(model);
		}
		model.addAttribute("users",  userService.findUserByAnyValue(user));
		return "allusers";
	}


	/*
	 * This method will be called to show user view being user's control panel
 	 * It also validates the user input
 	 */
	//TODO - checked
	@RequestMapping(value = { "/user-{id}" }, method = RequestMethod.GET)
	public String showUserView(ModelMap model, @PathVariable String id) {
		Optional<User> user =
				userService.findByIdWithMessagesAndItemsAssigned(Integer.parseInt(id));
		if (user.isPresent()){
			userService.simpleAuthentication(user.get());
			model.addAttribute("messages", user.get().getMessagesReceived());
			model.addAttribute("user", user.get());
			model.addAttribute("itemsToDo", user.get().getItemsAssigned());
		}
		return "userView";
	}

	/*
	 * This method will be called to show user messages view with messages
	 * received and messages sent
 	 */
	//TODO - checked
	@RequestMapping(value = { "/user-{id}/msg" }, method = RequestMethod.GET)
	public String showUserMessages(ModelMap model, @PathVariable String id) {
		Optional<User> user = userService.findByIdWithMessages(Integer.parseInt(id));
		if (user.isPresent()){
			userService.simpleAuthentication(user.get());
			model.addAttribute("messagesReceived", user.get().getMessagesReceived());
			model.addAttribute("messagesSent", user.get().getMessagesSent());
			model.addAttribute("user", user.get());
		}
		return "userMessages";
	}

	//TODO - checked
	@RequestMapping(value = { "/user-{id}/items" }, method = RequestMethod.GET)
	public String showOnlyUserItems(ModelMap model, @PathVariable String id) {
		Optional<User> user = userService.findByIdWithItemsAssigned(Integer.parseInt(id));
		if (user.isPresent()){
			userService.simpleAuthentication(user.get());
			Set<Item> allUserItemAssigned = user.get().getItemsAssigned();
			sortItemsByStateAndAddToModel(model, allUserItemAssigned);
			model.addAttribute("user", user.get());
		}
		return "userItems";
	}

//TODO - checked
	@RequestMapping(value = { "/user-{id}/allItems" }, method = RequestMethod.GET)
	public String showAllItemsToUser(ModelMap model, @PathVariable String id) {
		Optional<User> user = userService.findById(Integer.parseInt(id));
		if (user.isPresent()){
			userService.simpleAuthentication(user.get());
			Set<Item> allItemAssigned = new TreeSet<>(itemService.findAllItems());
			sortItemsByStateAndAddToModel(model, allItemAssigned);
			model.addAttribute("user", user.get());
			model.addAttribute("allUserView", "true");
		}
		return "userItems";
	}

	public ModelMap sortItemsByStateAndAddToModel(ModelMap model, Set<Item> itemsToBeSorted){
		model.addAttribute("newItems", userService.filterNewItems(itemsToBeSorted));
		model.addAttribute("activeItems", userService.filterActiveItems(itemsToBeSorted));
		model.addAttribute("resolvedItems", userService.filterResolvedItems(itemsToBeSorted));
		model.addAttribute("closedItems", userService.filterClosedItems(itemsToBeSorted));
		return model;
	}

	/*
 	* This method will handle new Message form request, adding
 	* link to the page from which the user requested new Message form.
 	*/
	//TODO - checked
	@RequestMapping(value = { "/user-{id}/newMsg" }, method = RequestMethod.GET)
	public String newMessageForm(ModelMap model, @PathVariable String id, HttpServletRequest request) {
		Optional<User> user =userService.findById(Integer.parseInt(id));
		if(user.isPresent()){
			userService.simpleAuthentication(user.get());
			model.addAttribute("user", user.get());
			model.addAttribute("message", new Message());
		}
		return "newMessage";
	}

	/*
 	* This method will handle new Message form POST request, validating the input
 	* and sending message if input is valid and receiver exists in database.
 	* Depending on sending result (sent/not sent) it will redirect with appropriate message
 	* to user messages view or show new message form again.
 	*/
	//TODO - checked
	@RequestMapping(value = { "/user-{id}/newMsg" }, method = RequestMethod.POST)
	public String sendNewMassage(ModelMap model, @PathVariable String id, @Valid Message message,
								 BindingResult result, HttpServletRequest request,
								 RedirectAttributes redirectAttributes) {
		String view="/user-{id}";
		// sender should not be null - will be set based on user-{id} - not user input
		Optional<User> sender = userService.findByIdWithMessages(Integer.parseInt(id));
		Optional<User> receiver= userService.findByLogin(message.getReceiver().getLogin());

		messageService.addSenderReceiverAndTimeInfo(message,sender, receiver);

		if (result.hasErrors() || !receiver.isPresent()) {
			if(result.hasErrors()){
				model.addAttribute("success", "Message has mistakes. Please insert correct " +
						"receiver's login and message's title and body.");
			} else {
				model.addAttribute("success", "There is no user with given login." +
						"Please try again");
			}
			view = newMessageForm(model,id, request);
		} else {
			if(messageService.saveMessage(message))	{
				model.addAttribute("success", "Message to "
						+ message.getReceiver().getLogin() + " was sent successfully!");
				view = showUserMessages(model,id);
			}
		}
		return view;
	}

	//TODO - checked
	@RequestMapping(value = { "/user-{id}/newItem" }, method = RequestMethod.GET)
	public String newItemForm(ModelMap model, @PathVariable String id, HttpServletRequest request) {
		Optional<User> user = userService.findById(Integer.parseInt(id));
		if(user.isPresent()){
			userService.simpleAuthentication(user.get());
			model.addAttribute("user", user.get());
			addItemFormValuesToModelWithAllItemStates(model);
		}
		addUrlPriorToVisitToSession(request);
		return "newItem";
	}

	//TODO - checked with assigned and without
	@RequestMapping(value = { "/user-{id}/newItem" }, method = RequestMethod.POST)
	public String addNewItem(@Valid Item item, BindingResult result,
							 ModelMap model, @PathVariable String id, HttpServletRequest request, RedirectAttributes redirectAttrs) {
		String redirectUrl = getUrlPriorToVisitFromSession(request);
		String view = "";
		Optional<User> assignedTo = itemService.getAndSetUserAssignedFromAssignedId(item);
		boolean addingItemPropertiesTest = itemService.addNewItemProperties(item, id);

		//checks if results has error or if item is assigned but he is not found in database
		if (result.hasErrors() || (item.getAssignedToId()!=0 && !assignedTo.isPresent())) {
			model.addAttribute("success", "Item has mistakes. Please insert correct " +
					"values and try again.");
			view = newItemForm(model, id,request);
		} else if (addingItemPropertiesTest){
			if (assignedTo.isPresent()){
				userNotificationService.sendEmailToUserAssignedToItem(assignedTo.get(), item);
			}
			itemService.saveItem(item);
			redirectAttrs.addFlashAttribute("success", "Item titled: "
					+ item.getTitle() + " added successfully");
			view = "redirect:" + redirectUrl;
		}
		return view;
	}

	public ModelMap loadInitialItemForm(ModelMap model){
		model.addAttribute("item", new Item());
		model.addAttribute("itemTypes", itemService.getItemTypeValues());
		model.addAttribute("itemPriorityList", itemService.getItemPriorityValues());
		model.addAttribute("itemSeverityList", itemService.getItemSeverityValues());
		return model;
	}

	public ModelMap addItemFormValuesToModelWithAllItemStates(ModelMap model) {
		loadInitialItemForm(model);
		model.addAttribute("itemStates", itemService.getAllItemStateValues());
		return model;
	}

	public ModelMap addItemFormValuesToModelForEditing(ModelMap model, Item item) {
		loadInitialItemForm(model);

		// if Item state is other than NEW, it can't be changed back to NEW
		if(item.getItemState()!=ItemState.NEW){
			model.addAttribute("itemStates", itemService.getItemStateValuesWithoutNew());
		} else{
			model.addAttribute("itemStates", itemService.getAllItemStateValues());
		}
		return model;
	}


//////////////////////         SPRING SECURITY CONTROLLER PART

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {
		return "login";
	}

	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

	@RequestMapping(value = "/access_denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("user", getPrincipal());
		return "accessDenied";
	}

	/*
	 * This method will returns user's username for Spring Security usage
 	 */
	private String getPrincipal(){
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			userName = ((UserDetails)principal).getUsername();
		} else {
			userName = principal.toString();
		}
		return userName;
	}

	@ModelAttribute("roles")
	public List<UserProfile> initializeProfiles() {
		return userProfileService.findAll();
	}

	@ModelAttribute("states")
	public List<UserState> initializeStates() {
		return userStateService.findAll();
	}

	private void printUserInfo(User user) {
		System.out.println("First Name : "+user.getFirstName());
		System.out.println("Last Name : "+user.getLastName());
		System.out.println("Login : "+user.getLogin());
		System.out.println("Password : "+user.getPassword());
		System.out.println("Email : "+user.getEmail());
		System.out.println("Checking UserProfiles....");
		if(user.getUserProfiles()!=null){
			for(UserProfile profile : user.getUserProfiles()){
				System.out.println("Profile : "+ profile.getType());
			}
		}
	}


//	@RequestMapping(value = "/admin", method = RequestMethod.GET)
//	public String adminPage(ModelMap model) {
//		model.addAttribute("user", getPrincipal());
//		return "admin";
//	}

//		TROUBLESHOOTING adding assignedTo User to new Item
//		//TODO delete after troubleshooting
//
//		System.out.println(result);
//		System.out.println(user.elaborateToString());
//
//		for (Object object : result.getAllErrors()) {
//			if(object instanceof FieldError) {
//				FieldError fieldError = (FieldError) object;
//
//				System.out.println(fieldError.getCode());
//			}
//
//			if(object instanceof ObjectError) {
//				ObjectError objectError = (ObjectError) object;
//
//				System.out.println(objectError.getCode());
//			}
//		}
//
//


}


