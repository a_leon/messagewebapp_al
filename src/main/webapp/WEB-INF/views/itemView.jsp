<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
    <title>Item View</title>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">

<%@include file="../jspf/menu.jspf"%>
    <div class="userPanel">

<%--<c:choose>--%>
    <%--<c:when test="${user!= null}">--%>
        <%--<h2>${user}</h2>--%>

 <%--</c:when>--%>
    <%--<c:otherwise>--%>
        <%--<h3>Brak usera o podanym id</h3>>--%>
    <%--</c:otherwise>--%>
<%--</c:choose>--%>

    <%--<div class="leftContainer">--%>
        <%--<h4>${user.id}. ${user.firstName} ${user.lastName}</h4>--%>
        <%--<p>Login: ${user.login}</p>--%>
        <%--<p>email: ${user.email}</p>--%>
        <%--<a href="<c:url value='/edit-${user.id}-user' />">Edit your account </a>--%>
    <%--</div>--%>
    <%@include file="../jspf/leftContainerWithUser.jspf"%>

    <div class="msgItemsContainer">
        <div class="messagesOrItems">
            <div class="msgOrItemTopBar">
                <h3>VIEW ITEM</h3>
                <ul  class="itemMsgMenu">

                <li>
                    <a href="<c:url value='/user-${user.id}/allItems' />">Go back to all items</a>
                </li>
                <li>
                    <a href="<c:url value='/user-${user.id}/items' />">Go back to user items</a>
                </li>
                </ul>
            </div>
    <c:choose>
    <c:when test="${item!= null}">
        <h2>Item ${item.itemId} - ${item.title}</h2>

        <%@include file="../jspf/editAndDeleteItemLinksShownIfUserIsAuthorized.jspf"%>

        <h3>Body: ${item.body} </h3>
        <h3>Priority: ${item.priority} </h3>
        <h3>Severity: ${item.severity} </h3>
        <h3>Type: ${item.type} </h3>
        <h3>Assigned to: ${item.assignedTo.id} ${item.assignedTo.firstName} ${item.assignedTo.lastName} </h3>
        <h3>Created time: ${item.createdTime} </h3>
    </c:when>
    <c:otherwise>
        <h3>Brak itemu o podanym id</h3>>
    </c:otherwise>
</c:choose>
        </div>
    </div>
    </div>
</div>

</body>
</html>
