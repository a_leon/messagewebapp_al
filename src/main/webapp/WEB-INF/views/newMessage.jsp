<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>New Message</title>
	<link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>

	<style>
		th {
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>

</head>
<body>
<div class="container">

<%@include file="../jspf/message.jspf"%>
<%@include file="../jspf/menu.jspf"%>

	<div class="userPanel">
		<c:choose>
		<c:when test="${user!= null}">
		<%@include file="../jspf/leftContainerWithUser.jspf"%>
			<div class="msgItemsContainer">
				<div class="messagesOrItems">
					<div class="msgOrItemTopBar">
						<h3>NEW MESSAGE FORM</h3>
						<ul  class="itemMsgMenu">
							<li>
								<a href="<c:url value='/user-${user.id}/msg' />">Go back to user messages </a>
							</li>
						</ul>
					</div>


<form:form method="POST" modelAttribute="message">
	<form:errors path="*" cssClass="error" element="div"/>
	<table>
		<tr>
			<td>To (login):	</td>
			<td><form:textarea path="receiver.login" rows="1" cols="30" /></td>
			<td><form:errors path="receiver.login" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Title</td>
			<td><form:textarea path="title" rows="1" cols="30"/></td>
			<td><form:errors path="title" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Body</td>
			<td><form:textarea path="body" rows="3" cols="30"/></td>
			<td><form:errors path="body" cssClass="error"/></td>
		</tr>
		<tr>
			<td colspan="3"><input type="submit"/></td>
		</tr>
	</table>
</form:form>

				</div>
			</div>
		</c:when>
			<c:otherwise>
				<h3>Brak usera o podanym id</h3>>
			</c:otherwise>
		</c:choose>
	</div>
</div>
<%--<a href="<c:url value='/user-${user.id}/msg' />">Go back to user messages </a>--%>
</body>
</html>