package pl.sda.springmvc.service;

import pl.sda.springmvc.model.Item;
import pl.sda.springmvc.model.User;

import java.util.Optional;

/**
 * Created by AJ on 2017-01-02.
 */
public interface UserNotificationService {

    boolean sendRegistrationConfirmation(User user);

    boolean sendForgottenPasswordEmail(User user);

    boolean sendMailToUserIfAssignedChanged
            (User user, Optional<Integer> assignedToIdPriorToChange, Item item);

    boolean sendEmailToUserAssignedToItem(User user,Item item);

    boolean sendChangedMailAddressEmail(User userOldMail, User userNewMail);
}
