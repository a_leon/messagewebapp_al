package pl.sda.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.springmvc.model.Item;
import pl.sda.springmvc.model.User;

import java.util.Optional;

/**
 * Created by AJ on 2017-01-02.
 */
@Service("registrationService")
public class UserNotificationServiceImpl implements UserNotificationService {
    @Autowired
    MailService mailService;

    @Autowired
    UserService userService;

    @Override
    public boolean sendRegistrationConfirmation(User user){
        return mailService.sendRegistrationEmail(user);
    }

    public boolean sendForgottenPasswordEmail(User user){
//        userService.setUsersNextUniqueEmailConfirmationId(user);
        return mailService.sendForgotPasswordEmail(user);
    }

    public boolean sendMailToUserIfAssignedChanged
            (User user, Optional<Integer> assignedToIdPriorToChange, Item item){
        boolean result = false;
        if(hasUserAssignedChanged(assignedToIdPriorToChange, item)){
            result = mailService.sendEmailToUserAssignedToItem(user, item);
        }
        return result;
    }

    private boolean hasUserAssignedChanged(Optional<Integer> assignedToIdBefore, Item item){
        boolean result = false;
        if(assignedToIdBefore.isPresent()){
            result = !assignedToIdBefore.get().equals(item.getAssignedToId());
        }
        return result;
    }

    public boolean sendEmailToUserAssignedToItem(User user,Item item){
        return mailService.sendEmailToUserAssignedToItem(user, item);
    }

    @Override
    public boolean sendChangedMailAddressEmail(User userWithOldEmail, User userWithNewEmail ) {
        boolean result = mailService.sendChangedMailOldAddressEmail(userWithOldEmail);
        return (mailService.sendChangedMailNewAddressEmail(userWithNewEmail) && result);
    }

}
