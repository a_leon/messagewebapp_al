package pl.sda.springmvc.dao;

import pl.sda.springmvc.model.Item;

import java.util.List;

public interface ItemDao {

	Item findById(int id);

	boolean saveItem(Item item);

	boolean deleteItemById(String id);
	
	List<Item> findAllItems();

	Item findItemById(Integer id);

}
