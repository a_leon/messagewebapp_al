<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Users</title>
	<link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
	<style>
		th {
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>

</head>
<body>
<div class="container">
    <%--<div class="header">--%>
        <%--<p>${success}</p>--%>
		<%--<c:if test="${users.size() == 0}">--%>
			<%--Brak userow!--%>
		<%--</c:if>--%>
    <%--</div>--%>
    <%@include file="../jspf/menu.jspf"%>

<c:if test="${searchList!= null}">
	<h3>Search results:</h3>
</c:if>

<table class="usersList">
	<thead>
	<tr>
		<th>User ID</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Login</th>
		<th>Email</th>
		<th>User State</th>
		<th colspan="2">User Menu</th>
		<%--<th></th>--%>
	</tr>
	</thead>
	<tbody>
	<c:forEach items="${users}" var="user">
		<tr>
			<td><c:out value="${user.getId()}" /></td>
			<td><c:out value="${user.getFirstName()}" /></td>
			<td><c:out value="${user.getLastName()}" /></td>
			<td><c:out value="${user.getLogin()}" /></td>
			<td><c:out value="${user.getEmail()}" /></td>
			<td><c:out value="${user.getState()}" /></td>
			<%--<td><a href="<c:url value='/user-${user.id}' />">User ${user.id} Menu </a></td>--%>
			<td><a href="<c:url value='/edit-${user.id}-user' />">Edit User ${user.id} </a></td>
			<td><a href="<c:url value='/delete-${user.id}-user' />">Delete</a></td>
		</tr>
	</c:forEach>
	</tbody>
</table>

<c:if test="${searchList!= null}">
	<form name="backToUsers" action="/list" method="get">
		<input type="submit" name="submit" value="Back to All Users"/>
	</form>
</c:if>

<%--<%if (searchList!=null) {%>--%>

<%--<form name="backToUsers" action="/users.html" method="post">--%>
	<%--<input type="submit" name="submit" value="Back"/>--%>
<%--</form>--%>
<%--<%}--%>
<%--%>--%>

</body>
</html>