<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>New Item</title>
	<link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
	<style>
		th {
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>

</head>
<body>
<div class="container">

<%@include file="../jspf/message.jspf"%>
<%@include file="../jspf/menu.jspf"%>
	<div class="userPanel">
		<c:choose>
		<c:when test="${user!= null}">
		<div class="leftContainer">
			<h4>${user.id}. ${user.firstName} ${user.lastName}</h4>
			<p>Login: ${user.login}</p>
			<p>email: ${user.email}</p>
			<a href="<c:url value='/edit-${user.id}-user' />">Edit your account </a>
		</div>
<%--<c:choose>--%>
	<%--<c:when test="${newMessageStatus!= null}">--%>
		<%--<h3>Message has not been sent. Please try again</h3>--%>
	<%--</c:when>--%>
	<%--<c:otherwise>--%>
		<%--<h3>New Message Form:</h3>--%>
	<%--</c:otherwise>--%>
<%--</c:choose>--%>
		<div class="msgItemsContainer">
			<div class="messagesOrItems">
				<div class="msgOrItemTopBar">
					<h3>NEW ITEM</h3>
					<ul  class="itemMsgMenu">

						<li>
							<a href="<c:url value='/user-${user.id}/allItems' />">Go back to all items</a>
						</li>
						<li>
							<a href="<c:url value='/user-${user.id}/items' />">Go back to user items</a>
						</li>
					</ul>
				</div>


<form:form method="POST" modelAttribute="item">
	<form:errors path="*" cssClass="error" element="div"/>
	<form:input type="hidden" path="itemState" id="itemState" value="NEW"/>

	<table>
		<%--<tr>--%>
			<%--<td>To:--%>
				<%--(firstName.lastName)--%>
			<%--</td>--%>
			<%--<td><form:input path="receiverFullName" rows="2" cols="30" /></td>--%>
			<%--<td><form:errors path="receiverFullName" cssClass="error"/></td>--%>
		<%--</tr>--%>
		<tr>
			<td>Title</td>
			<td><form:input path="title" rows="2" cols="30"/></td>
			<td><form:errors path="title" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Body</td>
			<td><form:input path="body" rows="5" cols="30"/></td>
			<td><form:errors path="body" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Item Type (BUG, TASK, FEATURE): </td>
			<td><form:select path="type">
					<form:options items="${itemTypes}" />
				</form:select>
			<td><form:errors path="type" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Item Priority (1-5): </td>
			<td><form:select path="priority">
					<form:options items="${itemPriorityList}" />
				</form:select>
			<td><form:errors path="priority" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Item Severity (1-3): </td>
			<td><form:select path="severity">
					<form:options items="${itemSeverityList}" />
				</form:select>
			<td><form:errors path="severity" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Assigned to (id): </td>
			<td><form:input path="assignedToId" id="assignedToId" /></td>
			<td><form:errors path="assignedToId" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Estimated time (in hours - format XX.X): </td>
			<td><form:input path="originalEstimate" /></td>
			<td><form:errors path="originalEstimate" cssClass="error"/></td>
		</tr>

			<tr>
				<td colspan="3"><input type="submit"/></td>
			</tr>
	</table>
</form:form>

<%--<a href="<c:url value='/user-${user.id}/items' />">Go back to user items </a>--%>
			</div>
		</div>
		</c:when>
			<c:otherwise>
				<h3>Brak usera o podanym id</h3>>
			</c:otherwise>
		</c:choose>
	</div>
</div>
</body>
</html>