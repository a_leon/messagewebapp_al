package pl.sda.springmvc.service;

/**
 * Created by AJ on 2017-01-02.
 */
public interface MailService {

    boolean sendRegistrationEmail(Object object);

    boolean sendForgotPasswordEmail(Object object);

    boolean sendEmailToUserAssignedToItem(Object object, Object object2);

    boolean sendChangedMailNewAddressEmail(Object object);

    boolean sendChangedMailOldAddressEmail(Object object);
}
