<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
<head>
    <title>User View</title>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
</head>
<body>

<div class="container">

<%@include file="../jspf/menu.jspf"%>

<div class="userPanel">
<c:choose>
    <c:when test="${user!= null}">

    <div class="leftContainer">
        <h4>${user.id}. ${user.firstName} ${user.lastName}</h4>
        <p>Login: ${user.login}</p>
        <p>email: ${user.email}</p>
        <a href="<c:url value='/edit-${user.id}-user' />">Edit your account </a>
    </div>
    <div class="msgItemsContainer">
        <div class="messagesOrItems">
            <div class="msgOrItemTopBar">
            <h3>MESSAGES RECEIVED</h3>
                <ul  class="itemMsgMenu">
                    <li>
                        <a href="<c:url value='/user-${user.id}/msg' />">All messages </a>
                    </li>
                    <li>
                        <a href="<c:url value='/user-${user.id}/newMsg' />">New message </a>
                    </li>
                </ul>
            </div>
            <div class="msgOrItemList">
                    <c:forEach items="${messages}" var="message" >
                        <%@include file="../jspf/singleMessageWithMenu.jspf"%>
                    </c:forEach>
                </div>
        </div>
        <div class="messagesOrItems">
            <div class="msgOrItemTopBar">
                <h3>ITEMS ASSIGNED</h3>
                <ul  class="itemMsgMenu">
                    <li>
                        <a href="<c:url value='/user-${user.id}/allItems' />">Show all company's items</a>
                    </li>
                    <li>
                        <a href="<c:url value='/user-${user.id}/items' />">Show my items</a>
                    </li>
                    <li>
                        <a href="<c:url value='/user-${user.id}/newItem' />">Add new item</a>
                    </li>
                </ul>
            </div>
            <div class="msgOrItemList">
            <c:forEach items="${itemsToDo}" var="item" >
                <%@include file="../jspf/singleItemWithMenu.jspf"%>
            </c:forEach>
            </div>
        </div>

    </c:when>
    <c:otherwise>
        <h3>Brak usera o podanym id</h3>>
    </c:otherwise>
</c:choose>
    </div>
</div>
</div>
</body>
</html>
