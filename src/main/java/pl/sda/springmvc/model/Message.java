package pl.sda.springmvc.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by AJ on 2016-12-05.
 */
@Entity
@Table(name = "message")
public class Message implements Serializable, Comparable<Message>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "msg_id")
    private Integer msg_id;

    @NotNull
    @Size(min=2, max=255)
    @Column(name = "title")
    private String title;

    @NotNull
    @Size(min=2)
    @Column(name = "body")
    private String body;

    @Column(name = "date")
    private Timestamp timestamp;

    @NotFound(action= NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "from_id")
    private User sender;

    @NotFound(action= NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "to_id")
    private User receiver;

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Message(){}

    public Message(String title, String body, User sender, User receiver) {
        this.title = title;
        this.body = body;
        this.sender = sender;
        this.receiver = receiver;
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
    }

    public int getMsg_id() {
        return msg_id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setMsg_id(int msg_id) {
        this.msg_id = msg_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Message{" +
                "msg_id=" + msg_id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", senderId=" + sender.getId() +
                ", receiverId=" + receiver.getId() +
                ", timestamp=" + timestamp +
                '}';
    }

    public String shortToString() {
        return "" + msg_id +
                ". " + title + "<br />" +
                "From: " + sender.getLogin() +
                " To: " + receiver.getLogin() +
                " Send: " + timestamp;
    }

    public int compareTo(Message o) {
        int result=o.getMsg_id() - this.msg_id;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (!title.equals(message.title)) return false;
        return body.equals(message.body);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + body.hashCode();
        return result;
    }

    //      FURTHER DEVELOPMENT OPTIONS
//    @Column(name = "group_id")
//    private int groupId;

//    @Column(name = "read")
//    private boolean read;
//
//    @Column(name = "archived")
//    private boolean archived;

//    @Column(name = "group")
//    private boolean groupMessage;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "group_id")
//    private User groupReceiver;



//    public void setGroupId(int groupId) {
//        this.groupId = groupId;
//    }

//    public boolean isRead() {
//        return read;
//    }
//
//    public boolean isArchived() {
//        return archived;
//    }

//    public int getGroupId() {
//        return groupId;
//    }

//    public String getReceiverFullName() {
//        return receiverFullName;
//    }
//
//    public void setReceiverFullName(String receiverFullName) {
//        this.receiverFullName = receiverFullName;
//    }

//    public Set<Message> getGroupMessages(int groupId)  {
//
//    }

//    public void setRead(boolean read) {
//        this.read = read;
//    }
//
//    public void setArchived(boolean archived) {
//        this.archived = archived;
//    }

}
