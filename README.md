# README #

## What is this repository for? ##

* This is a simple Message and ToDo list webapp using Spring (with Spring Security), Hibernate (with MySQL database) and JavaMail. The MessageWebApp_AL was made by Adam Leonarczyk as a final project for Software Development Academy course in Java.

## How do I get set up? ##

### 1. Tomcat configuration (using IntelliJ): ###
* add new configuration – tomcat local server
* deployment – deploy MessagingWebApp:war exploded   (naming might differ)
* before lunch - run maven goal (clean package)

### 2. AppConfig: ###
* mail server configuration – in getMailSender() - set login and password (currently gmail account Host and Port are set) 
* - mailSender.setUsername("XXXXXXXX");
* - mailSender.setPassword("XXXXXXXX");

### 3. Database configuration: ###
* Resources -> application_properties:
* jdbc.url = jdbc:mysql://localhost:3306/ messagewebapp  <- example new schema name 
* jdbc.username = XXXX			                 <- db username	
* jdbc.password = XXXX			                 <- db password

### 4. Database Initial Query: ###
* Please use query from src/main/resources/db_initial_query.txt file.

### 5. Run the application (available at http://localhost:8080/) 
Log in using login (admin, user1, user2) and password (admin123, user123, user321 respectively). *Hope you like it!

### Who do I talk to?

* Repo owner - Adam Leonarczyk


### Screenshots

[1. Login Page](http://imgur.com/V670x7h)
[2. User Main View](http://imgur.com/VobB7gy)
[3. User Items View](http://imgur.com/wyzf2rw)
[4. Password change mail](http://imgur.com/pZV2wi7)