package pl.sda.springmvc.service;

import pl.sda.springmvc.model.UserProfile;

import java.util.List;

/**
 * Created by AJ on 2016-12-30.
 */
public interface UserProfileService {

    List<UserProfile> findAll();

    UserProfile findByType(String type);

    UserProfile findById(int id);
}
