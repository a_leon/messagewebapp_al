<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Edit Item</title>
	<link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
	<style>
		th {
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>
</head>
<body>

<div class="container">

<%@include file="../jspf/message.jspf"%>
<%@include file="../jspf/menu.jspf"%>
	<div class="userPanel">

<%--<c:choose>--%>
	<%--<c:when test="${newMessageStatus!= null}">--%>
		<%--<h3>Message has not been sent. Please try again</h3>--%>
	<%--</c:when>--%>
	<%--<c:otherwise>--%>
		<%--<h3>New Message Form:</h3>--%>
	<%--</c:otherwise>--%>
<%--</c:choose>--%>

<form:form method="POST" modelAttribute="item">
	<form:errors path="*" cssClass="error" element="div"/>
	<form:input type="hidden" path="itemId" id="itemId"/>
	<table>
		<tr>
			<td><label for="title">Title</label></td>
			<td><form:input path="title" id="title" rows="2" cols="30"/></td>
			<td><form:errors path="title" cssClass="error"/></td>
		</tr>
		<tr>
			<td><label for="body">Body</label></td>
			<td><form:input path="body" id="body" rows="5" cols="30"/></td>
			<td><form:errors path="body" cssClass="error"/></td>
		</tr>
		<tr>
			<td><label for="itemState">Item State (NEW, ACTIVE, RESOLVED, COMPLETED):</label></td>
			<td><form:select path="itemState" >
				<form:options items="${itemStates}" />
				</form:select>
			<td><form:errors path="priority" cssClass="error"/></td>
			</tr>
		<tr>
			<td><label for="type">Item Type (BUG, TASK, FEATURE): </label></td>
			<td><form:select path="type" id="type">
					<form:options items="${itemTypes}" />
				</form:select>
			<td><form:errors path="type" cssClass="error"/></td>
		</tr>
		<tr>
			<td><label for="priority">Item Priority (1-5): </label></td>
			<td><form:select path="priority" id="priority">
					<form:options items="${itemPriorityList}" />
				</form:select>
			<td><form:errors path="priority" cssClass="error"/></td>
		</tr>
		<tr>
			<td><label for="severity">Item Severity (1-3): </label></td>
			<td><form:select path="severity" id="severity">
					<form:options items="${itemSeverityList}" />
				</form:select>
			<td><form:errors path="severity" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Assigned to (id): </td>
			<td><form:input path="assignedToId" id="assignedToId" /></td>
			<td><form:errors path="assignedToId" cssClass="error"/></td>
		</tr>
		<tr>
			<td><label for="remainingTime">Remaining time (in hours): </label></td>
			<td><form:input path="remainingTime" id="remainingTime"/></td>
			<td><form:errors path="remainingTime" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Time spent since last edit (in hours): </td>
			<td><form:input path="timeSpentSinceLastEdit" id="timeSpentSinceLastEdit" /></td>
			<td><form:errors path="timeSpentSinceLastEdit" cssClass="error"/></td>
		</tr>
		<tr>
			<td colspan="3"><input type="submit"/></td>
		</tr>
	</table>
</form:form>

<a href="<c:url value='/user-${user.id}/items' />">Go back to user items </a>
	</div>
</div>

</body>
</html>