package pl.sda.springmvc.service;

import org.springframework.expression.spel.ast.OpInc;
import org.springframework.security.access.prepost.PreAuthorize;
import pl.sda.springmvc.model.Item;
import pl.sda.springmvc.model.Message;
import pl.sda.springmvc.model.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {

	Optional<User> findById(int id);


	Optional<User> findByIdWithMessages(int id);

	void loadUsersMessagesReceivedAndItemsAssigned(User user);

	Optional<User> findByIdWithItemsAssigned(int id);

	Optional<User> findByIdWithItemsCreated(int id);


	Optional<User> findByIdWithMessagesAndItemsAssigned(int id);

	List<User> findUserByAnyValue(User user);

	Optional<User> findByLoginWithMsg(String receiver);		Optional<User> findByFirstAndLastName(String firstName, String lastName);

	Optional<User> findByFirstAndLastNameWithMessages(String firstName, String lastName);

	Optional<User> findByLogin(String login);

	Optional<User> findByLoginOrEmail(String login, String email);

	boolean saveUser(User user);
	
	void updateUser(User user);

	void setNewEncryptedPassword(User user, String password);

	boolean deleteUserById(Integer id);

	List<User> findAllUsers();

	void assignUserProfileToUserIfEmpty(User user);

	//TODO - future - modify - confirmationId should be stored as hashed - due to security measures
	void setUsersUniqueEmailConfirmationId(User user);

	void updateUserConfirmationId(User user);

	Optional<User> findUserByEmailConfirmationId(String confirmationId);

	Optional<User> findUserByEmail(String email);

	// method used only in controller for preauthorization of logged user
	// used userService in controller instead of getters in order to use PreAuthorize annotation
	// to block access to view only to specific logged user(authorized)
	@PreAuthorize("#user.getStringId()==authentication.name")
	boolean simpleAuthentication(User user);

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	boolean simpleAuthenticationWithIdStringOnly(String id);

	@PreAuthorize("hasRole('ROLE_ADMIN') or #user.getStringId()==authentication.name")
	boolean simpleAuthenticationWithAdminOrUser(User user);

	Set<Item> filterNewItems(Set<Item> items);

	Set<Item> filterActiveItems(Set<Item> items);

	Set<Item> filterResolvedItems(Set<Item> items);

	Set<Item> filterClosedItems(Set<Item> items);

//	Set<Message> getAllUserMessages(Integer id);

//	boolean isUserSsnUnique(Integer id, String ssn);


// 	@PreAuthorize("#user.login==authentication.name")
//	Set<Item> getUsersItemsAssigned(User user);
//
//	@PreAuthorize("#user.login==authentication.name")
//	Set<Message> getUsersMessagesReceived(User user);
//
//	@PreAuthorize("#user.login==authentication.name")
//	Set<Message> getUsersMessagesSent(User user);

}
