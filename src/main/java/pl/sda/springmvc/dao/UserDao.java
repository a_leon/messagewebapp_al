package pl.sda.springmvc.dao;

import java.util.List;


import pl.sda.springmvc.model.User;

public interface UserDao {

	User findById(int id);

	List<User> findUserByAnyValue(User user);

	User findByLogin(String login);

//	User findByIdWithMessagesReceived(int id);

//	List<Message> getMessagesReceived();

	User findByFirstAndLastName(String firstName, String lastName);

	boolean saveUser(User user);

	boolean deleteUserById(Integer id);
	
	List<User> findAllUsers();

	User findUserById(Integer id);

	User findByEmailConfirmationId(String confirmationId);

	User findByLoginOrEmail(String login, String email);

	User findByEmail(String email);

}
