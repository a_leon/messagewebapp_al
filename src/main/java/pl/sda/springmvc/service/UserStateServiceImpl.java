package pl.sda.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.springmvc.dao.UserProfileDao;
import pl.sda.springmvc.model.UserProfile;
import pl.sda.springmvc.model.UserState;

import java.util.List;

/**
 * Created by AJ on 2016-12-30.
 */
@Service("userStateService")
@Transactional
public class UserStateServiceImpl implements UserStateService{

    public List<UserState> findAll() {
        return UserState.getAllItemStateValues();
    }

//    public UserState findByType(String type){
//        return dao.findByType(type);
//    }
//
//    public UserState findById(int id) {
//        return dao.findById(id);
//    }

}
