<%--
  Created by IntelliJ IDEA.
  User: AJ
  Date: 2016-12-30
  Time: 03:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>User Registration Form</title>
    <link href="<c:url value='/static/css/application.css' />" rel="stylesheet"></link>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
</head>

<body>
<div class="container">

<%@include file="../jspf/menu.jspf"%>

<%--<c:choose>--%>
    <%--<c:when test="${success!= null}">--%>
        <%--<div class="header">--%>
            <%--<p>${success}</p>--%>
        <%--</div>--%>
    <%--</c:when>--%>
<%--</c:choose>--%>
    <div class="userPanel">

<div class="form-container">

    <h1>New User Registration Form</h1>

    <form:form method="POST" modelAttribute="user" class="form-horizontal">

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="firstName">First Name</label>
                <div class="col-md-7">
                    <form:input type="text" path="firstName" id="firstName" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="firstName" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="lastName">Last Name</label>
                <div class="col-md-7">
                    <form:input type="text" path="lastName" id="lastName" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="lastName" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="login">Login</label>
                <div class="col-md-7">
                    <form:input type="text" path="login" id="login" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="login" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <c:choose>
            <c:when test="${edit==null}">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable" for="password">Password</label>
                        <div class="col-md-7">
                            <form:input type="password" path="password" id="password" class="form-control input-sm"/>
                            <div class="has-error">
                                <form:errors path="password" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable" for="password">Confirm Password</label>
                        <div class="col-md-7">
                            <form:input type="password" path="confirmedPassword" id="confirmedPassword" class="form-control input-sm"/>
                            <div class="has-error">
                                <form:errors path="confirmedPassword" class="help-inline"/>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <form:input type="hidden" path="password" id="password" value="${user.password}"/>
            </c:otherwise>
        </c:choose>
        <div class="row">
            <div class="form-group col-md-12">
                <label class="col-md-3 control-lable" for="email">Email</label>
                <div class="col-md-7">
                    <form:input type="email" path="email" id="email" class="form-control input-sm"/>
                    <div class="has-error">
                        <form:errors path="email" class="help-inline"/>
                    </div>
                </div>
            </div>
        </div>

        <sec:authorize access="hasRole('ADMIN')">
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-lable" for="userProfiles">Roles</label>
                    <div class="col-md-7">
                        <form:select path="state" items="${states}" multiple="true" itemValue="state" itemLabel="state" class="form-control input-sm"/>
                        <div class="has-error">
                            <form:errors path="state" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label class="col-md-3 control-lable" for="userProfiles">Roles</label>
                    <div class="col-md-7">
                        <form:select path="userProfiles" items="${roles}" multiple="true" itemValue="id" itemLabel="type" class="form-control input-sm"/>
                        <div class="has-error">
                            <form:errors path="userProfiles" class="help-inline"/>
                        </div>
                    </div>
                </div>
            </div>
        </sec:authorize>
        <sec:authorize access="hasRole('USER')">
            <form:input type="hidden" path="state" id="state" value="${user.state}"/>
        </sec:authorize>

        <div class="row">
            <div class="form-actions floatRight">
                <input type="submit" value="Register" class="btn btn-primary btn-sm"> or <a href="<c:url value='/login' />">Cancel</a>
            </div>
        </div>
    </form:form>
    </div>

    <sec:authorize access="hasRole('ADMIN')">
        <a href="<c:url value='/list' />">Go back to user list</a>
    </sec:authorize>
    <sec:authorize access="hasRole('USER')">
        <c:choose>
            <c:when test="${edit= null}">
                <a href="<c:url value='/login' />">Go back to login</a>
            </c:when>
            <c:otherwise>
                <a href="<c:url value='/user-${user.id}' />">Go back to user view</a>
            </c:otherwise>
        </c:choose>
    </sec:authorize>

    </div>
</div>
</body>
</html>
