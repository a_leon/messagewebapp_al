package pl.sda.springmvc.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AJ on 2016-12-05.
 */
@Entity
@Table(name = "item")
public class Item implements Serializable, Comparable<Item>{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "item_id")
    private Integer itemId;

    @NotNull
    @Size(min=2, max=255)
    private String title;

    @NotNull
    @Size(min=2)
    @Column(name = "body")
    private String body;

    @Column(name = "item_type")
    @Enumerated(EnumType.STRING)
    private ItemType type;

    @NotNull
    private int priority;

    @NotNull
    private int severity;

    @NotFound(action= NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "createdBy_id")
    private User createdBy;

    @NotFound(action= NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "assignedTo_id")
    private User assignedTo;

    @Transient
    private int assignedToId;

    @Column(name = "item_state")
    @Enumerated(EnumType.STRING)
    private ItemState itemState;

    @Column(name = "created_time")
    private Timestamp createdTime; // check if valid type in relation to sql

    @Column(name = "modified_time")
    private Timestamp modifiedTime; // check if valid type in relation to sql

    //made spent times in BigDecimal as float is not recommended for use with database
    @Column(name = "original_estimate", columnDefinition="decimal", precision=4, scale=1)
    private BigDecimal originalEstimate;

    @Column(name = "remaining_time", columnDefinition="decimal", precision=4, scale=1)
    private BigDecimal remainingTime;

    @Transient
    private BigDecimal timeSpentSinceLastEdit;

    @Column(name = "total_time", columnDefinition="decimal", precision=4, scale=1)
    private BigDecimal totalTimeSpent;

    private static final int itemPriorityRange = 5;

    private static final int itemSeverityRange = 3;

    public Item(){}

    public Item(String title, String body, ItemType type, int priority, int severity, ItemState itemState) {
        this.title = title;
        this.body = body;
        this.type = type;
        this.priority = priority;
        this.severity = severity;
        this.itemState = itemState;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public BigDecimal getOriginalEstimate() {
        return originalEstimate;
    }

    public void setOriginalEstimate(BigDecimal originalEstimate) {
        this.originalEstimate = originalEstimate;
    }

    public BigDecimal getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(BigDecimal remainingTime) {
        this.remainingTime = remainingTime;
    }

    public BigDecimal getTotalTimeSpent() {
        return totalTimeSpent;
    }

    public void setTotalTimeSpent(BigDecimal totalTimeSpent) {
        this.totalTimeSpent = totalTimeSpent;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public User getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(User assignedTo) {
        this.assignedTo = assignedTo;
    }

    public ItemState getItemState() {
        return itemState;
    }

    public void setItemState(ItemState itemState) {
        this.itemState = itemState;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Timestamp modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public void updateTotalSpentTime(BigDecimal timeSpentSinceLastEdit){
        BigDecimal timeSpentSoFar = getTotalTimeSpent();

        if(timeSpentSoFar==null){
           timeSpentSoFar=new BigDecimal(0);
        }
        if(timeSpentSinceLastEdit==null){
            timeSpentSinceLastEdit =new BigDecimal(0);
        }

        BigDecimal sum = timeSpentSoFar.add(timeSpentSinceLastEdit);
        setTotalTimeSpent(sum);
    }

    public BigDecimal getTimeSpentSinceLastEdit() {
        return timeSpentSinceLastEdit;
    }

    public void setTimeSpentSinceLastEdit(BigDecimal timeSpentSinceLastEdit) {
        this.timeSpentSinceLastEdit = timeSpentSinceLastEdit;
    }

    public int getAssignedToId() {
        return assignedToId;
    }

    public void setAssignedToId(int assignedToId) {
        this.assignedToId = assignedToId;
    }

    public List<Integer> getSeverityOptions(){
        List<Integer> severityOptions = new ArrayList<>();
        for (int i=1; i<=this.itemSeverityRange; i++){
            severityOptions.add(i);
        }
        return severityOptions;
    }

    public List<Integer> getPriorityOptions(){
        List<Integer> priorityOptions = new ArrayList<>();
        for (int i=1; i<=this.itemPriorityRange; i++){
            priorityOptions.add(i);
        }
        return priorityOptions;
    }

    @Override
    public String toString() {
        return "Id. " + itemId +
                " " + title +
                " " + type +
                "   Priority: " + priority +
                "   Severity: " + severity +
                "   Assigned: " + assignedTo +
                "   State:" + itemState +
                "     Created:" + createdTime;
    }

    public String longToString() {
        return "Id. " + itemId +
                " " + title +
                 body + "\n" +
                " " + type +
                "   Priority: " + priority +
                "   Severity: " + severity +
                "   Assigned: " + assignedTo +
                "   " + itemState +
                ", original estimate=" + originalEstimate +
                ", remaining time=" + remainingTime +
                ", total time=" + totalTimeSpent +
                "     Created:" + createdTime
                + ", modifiedTime=" + modifiedTime
                ;
    }

    public int compareTo(Item o) {
        int result=o.getPriority()- this.priority;

        if(result!=0){
            return result;
        }

        result = o.getSeverity()- this.getSeverity();

        if (result!=0){
            return result;
        }

        Timestamp firstComparedTime = this.createdTime;
        Timestamp secondComparedTime = o.getCreatedTime();
        if (firstComparedTime==null && secondComparedTime!=null){
            result = 1;
        } else if (firstComparedTime==null && secondComparedTime==null){
            result = 0;
        } else if (firstComparedTime!=null && secondComparedTime==null){
            result = -1;
        } else {
            result = (int) this.createdTime.getTime()- (int) o.getCreatedTime().getTime();
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (priority != item.priority) return false;
        if (severity != item.severity) return false;
        if (!title.equals(item.title)) return false;
        return body.equals(item.body);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + body.hashCode();
        result = 31 * result + priority;
        result = 31 * result + severity;
        return result;
    }

    //    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "to_id")
//    private User receiver;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "group_id")
//    private User groupReceiver;

//    public User getReceiver() {
//        return receiver;
//    }
//
//    public void setReceiver(User receiver) {
//        this.receiver = receiver;
//    }


}
