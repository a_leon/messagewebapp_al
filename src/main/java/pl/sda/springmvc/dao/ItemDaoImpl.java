package pl.sda.springmvc.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.sda.springmvc.model.Item;

import java.util.List;

@Repository("itemDao")
public class ItemDaoImpl extends AbstractDao<Integer, Item> implements ItemDao {

	public Item findById(int id) {
		return getByKey(id);
	}

	public Item findItemById(int id){
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("item_id", id));
		return (Item) criteria.uniqueResult();
	}

	public boolean saveItem(Item item) {
		boolean result=false;
		try {
			persist(item);
			result=true;
		} catch (Exception e)	{
			e.printStackTrace();
		}
		return result;
	}

	public boolean deleteItemById(String id) {
		boolean result=false;
		try {
			Query query = getSession().createSQLQuery("delete from Item where item_id = :id");
			query.setString("id", id);
			query.executeUpdate();
			result = true;
		} catch (Exception e)	{
			e.printStackTrace();
		}
		return result;
	}

	public List<Item> findAllItems() {
		Criteria criteria = createEntityCriteria();
		return (List<Item>) criteria.list();
	}

	public Item findItemById(Integer id) {
		return getByKey(id);
	}

}
