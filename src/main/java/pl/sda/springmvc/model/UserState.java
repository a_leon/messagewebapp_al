package pl.sda.springmvc.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by AJ on 2016-12-17.
 */
public enum UserState {

    ACTIVE("Active"),
    INACTIVE("Inactive"),
    DELETED("Deleted"),
    LOCKED("Locked");

    private String state;

    UserState(final String state){
        this.state=state;
    }

    public String getState(){
        return this.state;
    }

    @Override
    public String toString() {
        return this.state;
    }

    public static List<UserState> getAllItemStateValues(){
        List<UserState> itemStates = new ArrayList<>(Arrays.asList(UserState.values()));
        return itemStates;
    }

    public String getName(){
        return this.name();
    }
}
