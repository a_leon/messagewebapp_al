<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Finding user</title>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">

<%@include file="../jspf/message.jspf" %>
<%@include file="../jspf/menu.jspf" %>
    <div class="userPanel">

    <p>Please enter your new password</p>

<form id="form" method="POST">
    <%--csrf token needed to be added as csrf attak prevention is switched on--%>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <table>
        <tr>
            <td>Login: </td>
            <td><input name="login" required="required" type="login" id="login" />
        </tr>
        <tr>
            <td>Password:</td>
            <td><input name="password" required="required" type="password" id="password" />
        </tr>
        <tr>
            <td>Confirm Password:</td>
            <td><input name="password_confirm" required="required" type="password" id="password_confirm" oninput="check(this)" />
                <script language='javascript' type='text/javascript'>
                    function check(input) {
                        if (input.value != document.getElementById('password').value) {
                            input.setCustomValidity('Passwords Must be Matching.');
                        } else {
                            // input is valid -- reset the error message
                            input.setCustomValidity('');
                        }
                    }
                </script>
        </tr>
        </table>
    <input type="submit" value="Submit">
</form>
    </div>
</div>
</body>
</html>