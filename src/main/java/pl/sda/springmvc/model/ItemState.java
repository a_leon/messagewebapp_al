package pl.sda.springmvc.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by AJ on 2016-12-17.
 */
public enum ItemState {

    NEW, ACTIVE, RESOLVED, CLOSED;

    public static List<ItemState> getItemStateValuesWithoutNew(){
        List<ItemState> itemStates = new ArrayList<>(Arrays.asList(ItemState.values()));
        itemStates.remove(NEW);
        return itemStates;
    }

    public static List<ItemState> getAllItemStateValues(){
        List<ItemState> itemStates = new ArrayList<>(Arrays.asList(ItemState.values()));
        return itemStates;
    }
}
