package pl.sda.springmvc.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by AJ on 2016-12-17.
 */
public enum UserProfileType {
    USER("USER"),
    ADMIN("ADMIN");

    String userProfileType;

    private UserProfileType(String userProfileType){
        this.userProfileType = userProfileType;
    }

    public String getUserProfileType(){
        return userProfileType;
    }

}
