package pl.sda.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import pl.sda.springmvc.model.Item;
import pl.sda.springmvc.model.User;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by AJ on 2017-01-02.
 */
@Service("mailService")
public class MailServiceImpl implements MailService {

    @Autowired
    JavaMailSender mailSender;

    private String urlDomain = "http://localhost:8080";

    public boolean sendForgotPasswordEmail(Object object) {
        return sendEmail(getForgotPasswordMessagePreparator((User) object));
    }

    public boolean sendRegistrationEmail(Object object) {
        return sendEmail(getRegistrationMessagePreparator((User) object));
    }

//    public boolean sendEmailToUserAssignedToItem(Object object) {
//        User user = (User) object;
//        return sendEmail(object, getItemAssignedToPreparator(user));
//    }


    public boolean sendEmailToUserAssignedToItem(Object object, Object object2) {
        return sendEmail(getItemAssignedToPreparator((User) object, (Item) object2));
    }

    public boolean sendChangedMailNewAddressEmail(Object object) {
        return sendEmail(getChangedMailNewAddressPreparator((User) object));
    }

    @Override
    public boolean sendChangedMailOldAddressEmail(Object object) {
        return sendEmail(getChangedMailOldAddressPreparator((User) object));
    }

    public boolean sendEmail(MimeMessagePreparator preparator) {

        boolean result = false;
        try {
            mailSender.send(preparator);
            result = true;
            System.out.println("Message Send...Success");
        } catch (MailException ex) {
            System.err.println(ex.getMessage());
        }
        return result;
    }


    private MimeMessagePreparator getForgotPasswordMessagePreparator(final User user) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setFrom("testforapptest4@gmail.com");
                mimeMessage.setRecipient(Message.RecipientType.TO,
                        new InternetAddress(user.getEmail()));
                mimeMessage.setText("Dear " + user.getFirstName()
                        + ",\n\n"
                        + "Your login is: " + user.getLogin()+ "\n"
                        + "Please go to the following link to change your password in MessageWebApp:"
                        +" "+ urlDomain +"/forgot/check/"+user.getEmailConfirmationId()
                        +"\n\nBest Regards,\nMessageWebApp Team");
                mimeMessage.setSubject("Forgotten password to MessageWebApp");
            }
        };
        return preparator;
    }

    private MimeMessagePreparator getRegistrationMessagePreparator(final User user) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setFrom("testforapptest4@gmail.com");
                mimeMessage.setRecipient(Message.RecipientType.TO,
                        new InternetAddress(user.getEmail()));
                mimeMessage.setText("Dear " + user.getFirstName()
                        + ",\nThank you for registration."
                        +"\nNew account in MessageWebApp was created with this email."
                        +" Please confirm your registration by clicking on the link below:"
                        +" "+ urlDomain +"/confirm/"+user.getEmailConfirmationId()
                        +"\n\nBest Regards,\nMessageWebApp Team");
                mimeMessage.setSubject("Your registration on MessageWebApp");
            }
        };
        return preparator;
    }

    private MimeMessagePreparator getItemAssignedToPreparator(final User user, final Item item) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setFrom("testforapptest4@gmail.com");
                mimeMessage.setRecipient(Message.RecipientType.TO,
                        new InternetAddress(user.getEmail()));
                mimeMessage.setText("Dear " + user.getFirstName()
                        + ",\nYou have been assigned to new Item."
                        + "\nItem title: " + item.getTitle() + "\nItem body: " + item.getBody()
                        + "\nTo view the Items assigned to you please follow the below link: "
                        +" "+ urlDomain +"/user-"+ user.getId() + "/items"
                        +"\n\nBest Regards,\nMessageWebApp Team");
                mimeMessage.setSubject("You've been assigned to new Item");
            }
        };
        return preparator;
    }

    private MimeMessagePreparator getChangedMailOldAddressPreparator(final User user) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setFrom("testforapptest4@gmail.com");
                mimeMessage.setRecipient(Message.RecipientType.TO,
                        new InternetAddress(user.getEmail()));
                mimeMessage.setText("Dear " + user.getFirstName()
                        + ",\nYour email at MessageWebApp has been changed."
                        +"\n\nPlease be informed that email address assigned to your account" +
                         " has been changed. Your account is currently inactive until new mail " +
                        "address is confirmed. \nIf change was not made by you please contact " +
                        "the site administrator.\n\nBest Regards,\nMessageWebApp Team");
                mimeMessage.setSubject("Changed email address on MessageWebApp");
            }
        };
        return preparator;
    }

    private MimeMessagePreparator getChangedMailNewAddressPreparator(final User user) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setFrom("testforapptest4@gmail.com");
                mimeMessage.setRecipient(Message.RecipientType.TO,
                        new InternetAddress(user.getEmail()));
                mimeMessage.setText("Dear " + user.getFirstName()
                        + ",\nYour email has been changed."
                        +"\nPlease confirm your new email by clicking on the link below:"
                        +" "+ urlDomain +"/confirm/"+user.getEmailConfirmationId()
                        +"\n\nBest Regards,\nMessageWebApp Team");
                mimeMessage.setSubject("Confirm your new mail on MessageWebApp");
            }
        };
        return preparator;
    }

}
