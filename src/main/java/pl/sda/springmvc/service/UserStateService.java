package pl.sda.springmvc.service;

import pl.sda.springmvc.model.UserProfile;
import pl.sda.springmvc.model.UserState;

import java.util.List;

/**
 * Created by AJ on 2016-12-30.
 */
//TODO - future - add UserStateDao - for now not necessary
public interface UserStateService {

    List<UserState> findAll();

//    UserState findByType(String type);
//
//    UserState findById(int id);
}
