<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Forgotten Password status</title>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link
</head>
<body>
<div class="container">

<%@include file="../jspf/message.jspf" %>
<%@include file="../jspf/menu.jspf" %>
    <div class="userPanel">

<%--<c:choose>--%>
    <%--<c:when test="${newPasswordSuccess}!= null}">--%>
        <%--<h3>Message has not been sent. Please try again</h3>--%>
    <%--</c:when>--%>
    <%--<c:otherwise>--%>
        <%--<h3>New Message Form:</h3>--%>
    <%--</c:otherwise>--%>
<%--</c:choose>--%>




<a href="<c:url value='/login' />">Go back to login</a>

<c:choose>
    <c:when test="${newPasswordSuccess}== null}">
        <a href="<c:url value='/forgot' />">Change my password</a>
</c:when>
<%--<c:otherwise>--%>
<%--<h3>New Message Form:</h3>--%>
<%--</c:otherwise>--%>
</c:choose>
    </div>
</div>
</body>
</html>