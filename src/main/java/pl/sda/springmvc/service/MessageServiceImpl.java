package pl.sda.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.springmvc.dao.MessageDao;
import pl.sda.springmvc.model.Message;
import pl.sda.springmvc.model.User;


import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service("messageService")
@Transactional
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageDao dao;

	@Autowired
	private UserService userService;

	
	public Optional<Message> findById(int id) {
		return Optional.ofNullable(dao.findById(id));
	}

	public boolean saveMessage(Message message) {
		return dao.saveMessage(message);
	}


	public boolean deleteMessageById(Integer id) {
		return dao.deleteMessageById(String.valueOf(id));
	}

	public List<Message> findAllMessages() {
		return dao.findAllMessages();
	}

	public void addSenderReceiverAndTimeInfo(Message message, Optional<User> sender, Optional<User> receiver){
		message.setSender(sender.get());
		if (receiver.isPresent()){
			message.setReceiver(receiver.get());
		}
		message.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
	}

	
}
