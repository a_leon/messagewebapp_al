package pl.sda.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.springmvc.model.User;
import pl.sda.springmvc.model.UserProfile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by AJ on 2016-12-30.
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<User> userOptional = userService.findByLogin(login);

        System.out.println("User : "+ userOptional.get());
        if (!userOptional.isPresent()){
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }
        User user = userOptional.get();

        return new org.springframework.security.core.userdetails.User(String.valueOf(user.getId()), user.getPassword(),
                user.getState().equals("Active"), true, true, true ,getGrantedAuthorities(user));

        //Below is version with userLogin as username
        //        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(),
        //                user.getState().equals("Active"), true, true, true ,getGrantedAuthorities(user));
    }

    private List<GrantedAuthority> getGrantedAuthorities(User user){
        List<GrantedAuthority> authorities = new ArrayList<>();

        for(UserProfile userProfile : user.getUserProfiles()){
            System.out.println("UserProfile : "+userProfile);
            authorities.add(new SimpleGrantedAuthority("ROLE_"+ userProfile.getType()));
        }
        System.out.print("authorities :"+authorities);
        return authorities;
    }

}
