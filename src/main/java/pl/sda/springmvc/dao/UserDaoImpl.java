package pl.sda.springmvc.dao;

import java.util.List;


import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.sda.springmvc.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	public User findById(int id) {
		return getByKey(id);
	}

	public User findUserById(Integer id) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("id", id));
		return (User) criteria.uniqueResult();
	}

	public User findByFirstAndLastName(String firstName, String lastName) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("firstName", firstName));
		criteria.add(Restrictions.eq("lastName", lastName));
		return (User) criteria.uniqueResult();
	}

	public boolean saveUser(User user) {
		boolean result=false;
		try {
			persist(user);
			result=true;
		} catch (Exception e)	{
			e.printStackTrace();
		}
		return result;
	}

	public boolean deleteUserById(Integer id) {
		boolean result=false;
		try {
			Criteria crit = createEntityCriteria();
			crit.add(Restrictions.eq("id", id));
			User user = (User)crit.uniqueResult();
			delete(user);
//			Query query = getSession().createSQLQuery("delete from User where user_id = :id");
//			query.setString("id", String.valueOf(id));
//			query.executeUpdate();

			result=true;
		} catch (Exception e){
			e.printStackTrace();
		}
		return result;
	}

	//TODO check if can be changed
	public List<User> findUserByAnyValue(User user) {
		Integer intId= user.getId();
		String firstName = user.getFirstName();
		String lastName = user.getLastName();

		Criteria criteria = createEntityCriteria();

		if (intId!=0 && intId!=null ) {
			criteria.add(Restrictions.eq("id", intId));
		}

		if (!firstName.trim().equals("") && firstName!=null)	{
			criteria.add(Restrictions.eq("firstName", firstName));
		}

		if (!lastName.trim().equals("") && lastName!=null)	{
			criteria.add(Restrictions.eq("lastName", lastName));
		}

		return (List<User>) criteria.list();
	}

	public User findByLoginOrEmail(String login, String email){
		Criteria criteria = createEntityCriteria();
		criteria.add( Restrictions.or(
				Restrictions.eq("login", login),
				Restrictions.eq("email", email)));
		return (User) criteria.uniqueResult();
	}


	public User findByLogin(String login){
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("login", login));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User findByEmail(String email) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("email", email));
		return (User) criteria.uniqueResult();
	}

	public User findByEmailConfirmationId(String confirmationId){
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("emailConfirmationId", confirmationId));
		return (User) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<User> findAllUsers() {
		Criteria criteria = createEntityCriteria();
		return (List<User>) criteria.list();
	}


//	public User findByIdWithMessagesReceived(int id) {
//		User user = findUserById(id);
//		Hibernate.initialize(user.getMessagesReceived());
//		return user;
//	}
//
//	public User findByIdWithMessagesSent(int id) {
//		User user = findUserById(id);
//		Hibernate.initialize(user.getMessagesSent());
//		return user;
//	}

}
