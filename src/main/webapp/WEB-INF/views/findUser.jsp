<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Finding user</title>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">
<%@include file="../jspf/message.jspf" %>
<%@include file="../jspf/menu.jspf" %>
    <div class="userPanel">

<p>If id is not known please leave 0 value</p>

<form:form method="POST" modelAttribute="user">
    <table>
        <tr>
            <td><label for="id">Id: </label> </td>
            <td><form:input path="id" id="id" required="required" pattern="^[0-9]+$"/></td>
            <td><form:errors path="id" cssClass="error"/></td>
        </tr>
        <tr>
            <td><label for="firstName">First name: </label> </td>
            <td><form:input path="firstName" id="firstName"/></td>
            <td><form:errors path="firstName" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="lastName">Last name: </label> </td>
            <td><form:input path="lastName" id="lastName"/></td>
            <td><form:errors path="lastName" cssClass="error"/></td>
        </tr>

        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
    </div>
</div>
</body>
</html>