package pl.sda.springmvc.dao;

import pl.sda.springmvc.model.Message;

import java.util.List;


public interface MessageDao {

	Message findById(int id);

	boolean saveMessage(Message message);

	boolean deleteMessageById(String id);
	
	List<Message> findAllMessages();

	Message findMessageById(Integer id);

}
