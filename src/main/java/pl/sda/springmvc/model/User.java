package pl.sda.springmvc.model;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 */

@Entity
@Table(name="user")
public class User implements Comparable<User> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="user_id")
	private int id;

	@NotNull
	@Size(min=2, max=40)
	@Column(name="first_name")
	private String firstName;

	@NotNull
	@Size(min=2, max=40)
	@Column(name = "last_name")
	private String lastName;

	@NotNull
	@Size(min=2, max=40)
	@Column(unique = true)
	private String login;

	@NotNull
	@Size(min=2, max=100)
	private String password;

	@Transient
	private String confirmedPassword;

	@NotNull
	@Column(unique = true)
	@Size(min=2, max=30)
	private String email;

	@Column(name = "confirmationId")
	private String emailConfirmationId;

	@NotNull
	private String state = UserState.INACTIVE.getState();

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "USER_USER_PROFILE",
			joinColumns = { @JoinColumn(name = "USER_ID") },
			inverseJoinColumns = { @JoinColumn(name = "USER_PROFILE_ID") })
	private Set<UserProfile> userProfiles = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sender")
	@OrderBy
	private Set<Message> messagesSent = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "receiver")
	@OrderBy
	private Set<Message> messagesReceived = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "createdBy")
	private Set<Item> itemsCreated = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "assignedTo")
	private Set<Item> itemsAssigned = new TreeSet<>();

	public User()	{
	}

	public User(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	// String representation of id - used to authenticate based on id value
	// compared to authentication.principal - userService.simpleAuthentication
	// String.valueOf doesn't work in PreAuthorize annotation
	// id value in authentication necessary redirect to proper userView with userId after login
	public String getStringId() {
		return String.valueOf(getId());
	}

	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmedPassword() {
		return confirmedPassword;
	}

	public void setConfirmedPassword(String confirmedPassword) {
		this.confirmedPassword = confirmedPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Set<UserProfile> getUserProfiles() {
		return userProfiles;
	}

	public void setUserProfiles(Set<UserProfile> userProfiles) {
		this.userProfiles = userProfiles;
	}

	public Set<Message> getAllUserMessages()	{
		Set<Message> allUserMessages = new TreeSet<>(this.getMessagesReceived());
		allUserMessages.addAll(this.getMessagesSent());
		return allUserMessages;
	}

	public Set<Item> getItemsCreated() {
		return itemsCreated;
	}

	public void setItemsCreated(Set<Item> itemsCreated) {
		this.itemsCreated = itemsCreated;
	}

	public Set<Item> getItemsAssigned() {
		return new TreeSet<>(itemsAssigned);
	}

	public void setItemsAssigned(Set<Item> itemsAssigned) {
		this.itemsAssigned = itemsAssigned;
	}

	public Set<Message> getMessagesReceived() {
		return new TreeSet<>(this.messagesReceived);
	}

	public void setMessagesReceived(Set<Message> messagesReceived) {
		this.messagesReceived = messagesReceived;
	}

	public Set<Message> getMessagesSent() {
		return messagesSent;
	}

	public void setMessagesSent(Set<Message> messagesSent) {
		this.messagesSent = messagesSent;
	}

	public String getEmailConfirmationId() {
		return emailConfirmationId;
	}

	public void setEmailConfirmationId(String emailConfirmationId) {
		this.emailConfirmationId = emailConfirmationId;
	}

	@Override
	public String toString() {
		return "" + id +
				". " + firstName + " " + lastName +
				"<br/>" + "Login: " + login +
				"<br/>"+ "email: " + email;
	}

	public String shortToString() {
		return "User " + id +
				". " + firstName + " " + lastName
				;
	}

	public String elaborateToString() {
		return "User{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", login='" + login + '\'' +
				", password='" + password + '\'' +
				", confirmedPassword='" + confirmedPassword + '\'' +
				", email='" + email + '\'' +
				", emailConfirmationId='" + emailConfirmationId + '\'' +
				", state='" + state + '\'' +
				'}';
	}

	public int compareTo(User o) {
		return (int) this.id- (int) o.getId();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		User user = (User) o;

		if (!firstName.equals(user.firstName)) return false;
		if (!lastName.equals(user.lastName)) return false;
		if (!login.equals(user.login)) return false;
		return email.equals(user.email);
	}

	@Override
	public int hashCode() {
		int result = firstName.hashCode();
		result = 31 * result + lastName.hashCode();
		result = 31 * result + login.hashCode();
		result = 31 * result + email.hashCode();
		return result;
	}

//	public Set<Message> getArchivedSentMessages()	{
//		Set<Message> archivedSentMessages= new HashSet<>();
//		Set<Message> sentMessages=this.getMessagesSent();
//		for (Message msg : sentMessages)	{
//			if (msg.isArchived())	{
//				archivedSentMessages.add(msg);
//			}
//		}
//		return archivedSentMessages;
//	}

}
