package pl.sda.springmvc.service;

import pl.sda.springmvc.model.Message;
import pl.sda.springmvc.model.User;

import java.util.List;
import java.util.Optional;

public interface MessageService {

	Optional<Message> findById(int id);

//	Optional<Message> createNewMessageFromForm(MessageFormDTO messageFormDTO, String id);
	
	boolean saveMessage(Message message);
	
//	void updateMessage(Message message);
	
	boolean deleteMessageById(Integer id);

	List<Message> findAllMessages();

	void addSenderReceiverAndTimeInfo(Message message, Optional<User> sender, Optional<User> receiver);
}
