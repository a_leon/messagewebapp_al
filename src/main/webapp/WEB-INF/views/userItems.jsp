<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
    <title>User</title>
    <link href="<c:url value='/static/css/styleUserItems.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">
    <%@include file="../jspf/menu.jspf"%>

        <div class="userPanel">

<c:choose>
    <c:when test="${user!= null}">
    <div class="leftContainer">
        <h4>${user.id}. ${user.firstName} ${user.lastName}</h4>
        <p>Login: ${user.login}</p>
        <p>email: ${user.email}</p>
        <a href="<c:url value='/edit-${user.id}-user' />">Edit your account </a>
    </div>
    <div class="msgItemsContainer">
        <div class="msgOrItemTopBar">
            <c:choose>
                <c:when test="${allUserView!= null}">
                    <h3>All Company Items</h3>
                </c:when>
                <c:otherwise>
                    <h3>${user.firstName} ${user.lastName} all items</h3>
                </c:otherwise>
            </c:choose>
            <ul  class="itemMsgMenu">
                <li>
                    <a href="<c:url value='/user-${user.id}/allItems' />">Show all company's items</a>
                </li>
                <li>
                    <a href="<c:url value='/user-${user.id}/newItem' />">Add new item</a>
                </li>
                <li>
                    <a href="<c:url value='/user-${user.id}' />">Go back to user view</a>
                </li>
            </ul>
        </div>
    <div class="itemByStateBox">
        <h3>New Items</h3>
        <div class="itemsByStateList">
        <c:forEach items="${newItems}" var="item" >
            <%@include file="../jspf/singleItemWithMenuShort.jspf"%>
        </c:forEach>
        </div>
    </div>
    <div class="itemByStateBox">
        <h3>Active Items</h3>
        <div class="itemsByStateList">
        <c:forEach items="${activeItems}" var="item" >
            <%@include file="../jspf/singleItemWithMenuShort.jspf"%>
        </c:forEach>
        </div>
    </div>
    <div class="itemByStateBox">
        <h3>Resolved Items</h3>
        <div class="itemsByStateList">
        <c:forEach items="${resolvedItems}" var="item" >
            <%@include file="../jspf/singleItemWithMenuShort.jspf"%>
        </c:forEach>
        </div>
    </div>
    <div class="itemByStateBox">
        <h3>Closed Items</h3>
        <div class="itemsByStateList">
        <c:forEach items="${closedItems}" var="item" >
            <%@include file="../jspf/singleItemWithMenuShort.jspf"%>
        </c:forEach>
        </div>
    </div>
    </c:when>
    <c:otherwise>
        <h3>Brak usera o podanym id</h3>>
    </c:otherwise>
</c:choose>

</body>
</html>
