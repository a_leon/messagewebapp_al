<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
    <title>User Messages</title>
    <link href="<c:url value='/static/css/styleUserMessages.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">

<%@include file="../jspf/menu.jspf"%>
    <div class="userPanel">
<c:choose>
    <c:when test="${user!= null}">
    <div class="leftContainer">
        <h4>${user.id}. ${user.firstName} ${user.lastName}</h4>
        <p>Login: ${user.login}</p>
        <p>email: ${user.email}</p>
        <a href="<c:url value='/edit-${user.id}-user' />">Edit your account </a>
    </div>
    <div class="msgItemsContainer">
        <div class="msgOrItemTopBar">
            <ul  class="itemMsgMenu">
                <li>
                    <a href="<c:url value='/user-${user.id}/newMsg' />">New message </a>
                </li>
                <li>
                    <a href="<c:url value='/user-${user.id}' />">Go back to user view</a>
                </li>
            </ul>
        </div>
        <div class="messagesOrItems">
            <div class="msgOrItemTopBarSmall">
            <h3>MESSAGES RECEIVED</h3>
            </div>
                <div class="msgOrItemList">
            <c:choose>
            <c:when test="${messagesReceived.size()!=0}">
                <c:forEach items="${messagesReceived}" var="message" >
                <%@include file="../jspf/singleMessageWithMenu.jspf"%>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <h3>No messages received</h3>
            </c:otherwise>
        </c:choose>
        </div>
        </div>
        <div class="messagesOrItems">
            <div class="msgOrItemTopBarSmall">
            <h3>MESSAGES SENT</h3>
            </div>
            <div class="msgOrItemList">
            <c:choose>
                <c:when test="${messagesSent.size()!=0}">
                    <c:forEach items="${messagesSent}" var="message" >
                        <%@include file="../jspf/singleMessageWithMenu.jspf"%>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <h4>No messages sent</h4>
                </c:otherwise>
            </c:choose>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <h4>Brak usera o podanym id</h4>>
    </c:otherwise>
</c:choose>
    </div>
</div>
</div>
</body>
</html>
