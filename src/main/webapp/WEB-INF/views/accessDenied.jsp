<%--
  Created by IntelliJ IDEA.
  User: AJ
  Date: 2016-12-31
  Time: 03:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>AccessDenied page</title>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">
    <%@include file="../jspf/menu.jspf"%>
        <div class="userPanel">
Dear <strong>${user}</strong>, You are not authorized to access this page
<a href="<c:url value="/user-${user}" />">Go back to user view</a>
<%--<a href="<c:url value="/logout" />">Logout</a>--%>
        </div>
</div>
</body>
</html>