<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Finding user</title>
    <link href="<c:url value='/static/css/style.css' />" rel="stylesheet"></link>
</head>
<body>
<div class="container">

<%@include file="../jspf/message.jspf" %>
<%@include file="../jspf/menu.jspf" %>
    <div class="userPanel">


<p>Please enter your email to which we will send a link to password changing site</p>

<form id="form" method="POST">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    email: <input type="email" name="email">
    <input type="submit" value="Submit">
</form>

<a href="<c:url value='/login' />">Go back to login</a>
    </div>
</div>
</body>
</html>