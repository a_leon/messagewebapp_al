package pl.sda.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.springmvc.dao.ItemDao;
import pl.sda.springmvc.model.*;

import javax.swing.text.html.Option;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service("itemService")
@Transactional
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemDao itemDao;

	@Autowired
	private UserService userService;

	@Autowired
	private MailService mailService;

	
	public Optional<Item> findById(int id) {
		return Optional.ofNullable(itemDao.findItemById(id));
	}

	@Override
	public boolean saveItem(Item item) {
		return itemDao.saveItem(item);
	}

	@Override
	public List<ItemState> getAllItemStateValues() {
		return ItemState.getAllItemStateValues();
	}

	@Override
	public List<ItemState> getItemStateValuesWithoutNew() {
		return ItemState.getItemStateValuesWithoutNew();
	}

	@Override
	public List<ItemType> getItemTypeValues() {
		return ItemType.getItemTypeValues();
	}

	@Override
	public List<Integer> getItemPriorityValues() {
		return new Item().getPriorityOptions();
	}

	@Override
	public List<Integer> getItemSeverityValues() {
		return new Item().getSeverityOptions();
	}


	/*
	* This method adds properties to Item which are not
	* automatically added by Spring form
	*/
	public boolean addNewItemProperties(Item item, String id) {
		boolean result = false;
		Optional<User> createdByUser = userService.findById(Integer.parseInt(id));

		if (createdByUser.isPresent()) {
			item.setCreatedBy(createdByUser.get());
			item.setCreatedTime(Timestamp.valueOf(LocalDateTime.now()));
			item.setRemainingTime(item.getOriginalEstimate());
			result = true;
		}
		return result;
	}

	/*
	* This method checks if user was assigned to new Item
	 * If true - it will check if user with passed Id exists.
	 * Method return false only when assigned user does not exists -
	 * indicating if the proper user assignment test is passed
	*/
	public Optional<User> getAndSetUserAssignedFromAssignedId(Item item){
		int assignedToId = item.getAssignedToId();
		Optional<User> assignedTo = Optional.empty();
		if(assignedToId!=0) {
			assignedTo = userService.findById(assignedToId);
			if (assignedTo.isPresent()) {
				item.setAssignedTo(assignedTo.get());
			}
		} else{
			item.setAssignedTo(null);
		}
		return assignedTo;
	}

	@Override
	public boolean deleteItemById(Integer id) {
		return itemDao.deleteItemById(String.valueOf(id));
	}

	@Override
	public List<Item> findAllItems() {
		return itemDao.findAllItems();
	}

	public void updateItem(Item item) {
		Optional<Item> entity = findById(item.getItemId());
		if(entity.isPresent()){
			entity.get().setTitle(item.getTitle());
			entity.get().setBody(item.getBody());
			entity.get().setAssignedTo(item.getAssignedTo());
			entity.get().setItemState(item.getItemState());
			entity.get().setPriority(item.getPriority());
			entity.get().setSeverity(item.getSeverity());
			entity.get().setRemainingTime(item.getRemainingTime());
			entity.get().updateTotalSpentTime(item.getTotalTimeSpent());
			entity.get().setModifiedTime(Timestamp.valueOf(LocalDateTime.now()));
		}
	}


//	public User findUserById(Integer id) {
//		return itemDao.findUserById(id);
//	}
	
}
