package pl.sda.springmvc.service;

import org.springframework.security.access.prepost.PreAuthorize;
import pl.sda.springmvc.model.*;

import java.util.List;
import java.util.Optional;

public interface ItemService {

	Optional<Item> findById(int id);

//	Optional<Item> createNewItem(MessageFormDTO messageFormDTO, String id);
	
	boolean saveItem(Item item);

	List<ItemState> getItemStateValuesWithoutNew();

	List<ItemState> getAllItemStateValues();

	List<ItemType> getItemTypeValues();

	List<Integer> getItemPriorityValues();

	List<Integer> getItemSeverityValues();

//	boolean updateItemFromDTOForm(ItemFormDTO itemFormDTO);
//
//	ItemFormDTO createItemFormDTOFromItem(Item item);

	void updateItem(Item item);

//	void updateItem(Item message);
	
	boolean deleteItemById(Integer id);

	List<Item> findAllItems();

	boolean addNewItemProperties(Item item, String id);

	Optional<User> getAndSetUserAssignedFromAssignedId(Item item);


}
