package pl.sda.springmvc.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;

import org.springframework.stereotype.Repository;
import pl.sda.springmvc.model.Message;

import java.util.List;

@Repository("messageDao")
public class MessageDaoImpl extends AbstractDao<Integer, Message> implements MessageDao {

	public Message findById(int id) {
		return getByKey(id);
	}

	public boolean saveMessage(Message message) {
		boolean result=false;
		try {
			persist(message);
			result=true;
		} catch (Exception e)	{
			e.printStackTrace();
		}
		return result;
	}

	public boolean deleteMessageById(String id) {
		boolean result=false;
		try {
			Query query = getSession().createSQLQuery("delete from Message where msg_id = :id");
			query.setString("id", id);
			query.executeUpdate();
			result = true;
		} catch (Exception e)	{
			e.printStackTrace();
		}
		return result;
	}

	public List<Message> findAllMessages() {
		Criteria criteria = createEntityCriteria();
		return (List<Message>) criteria.list();
	}

	public Message findMessageById(Integer id) {
		return getByKey(id);
	}

}
