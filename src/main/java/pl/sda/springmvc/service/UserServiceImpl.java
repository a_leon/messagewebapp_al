package pl.sda.springmvc.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.springmvc.dao.UserDao;

import pl.sda.springmvc.model.Item;
import pl.sda.springmvc.model.ItemState;
import pl.sda.springmvc.model.User;
import pl.sda.springmvc.model.UserProfile;

import java.util.*;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public Optional<User> findById(int id) {
		return Optional.ofNullable(dao.findUserById(id));
	}

	public Optional<User> findByIdWithMessages(int id) {
		Optional<User> user =  Optional.ofNullable(dao.findUserById(id));
		if(user.isPresent())	{
			Hibernate.initialize(user.get().getMessagesReceived());
			Hibernate.initialize(user.get().getMessagesSent());
		}
		return user;
	}

	public void loadUsersMessagesReceivedAndItemsAssigned(User user){
		Hibernate.initialize(user.getMessagesReceived());
		Hibernate.initialize(user.getItemsAssigned());
	}

	public Optional<User> findByLogin(String login){
		return Optional.ofNullable(dao.findByLogin(login));
	}


	public boolean saveUser(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return dao.saveUser(user);
	}

	/*
         * Since the method is running with Transaction, No need to call hibernate update explicitly.
         * Just fetch the entity from db and update it with proper values within transaction.
         * It will be updated in db once transaction ends.
         */
	public void updateUser(User user) {
		User entity = dao.findById(user.getId());
		if(entity!=null){
			entity.setFirstName(user.getFirstName());
			entity.setLastName(user.getLastName());
			entity.setLogin(user.getLogin());
			entity.setEmail(user.getEmail());
			entity.setPassword(user.getPassword());
			entity.setEmailConfirmationId(user.getEmailConfirmationId());
			entity.setState(user.getState());
		}
	}

	public void updateUserConfirmationId(User user) {
		User entity = dao.findById(user.getId());
		if(entity!=null){
			entity.setEmailConfirmationId(user.getEmailConfirmationId());
		}
	}

	public void setNewEncryptedPassword(User user, String password){
		User entity = dao.findById(user.getId());
		if(entity!=null){
			entity.setPassword(passwordEncoder.encode(password));
		}
	}

	public void assignUserProfileToUserIfEmpty(User user){
		if(user.getUserProfiles().isEmpty()) {
			Set<UserProfile> userProfile = new HashSet<>();
			userProfile.add(userProfileService.findById(1));
			user.setUserProfiles(userProfile);
		}
	}

	public List<User> findUserByAnyValue(User user){
		return dao.findUserByAnyValue(user);
	}

	public boolean deleteUserById(Integer id) {
		return dao.deleteUserById(id);
	}
	
	public List<User> findAllUsers() {
		return dao.findAllUsers();
	}

	public Optional<User> findByFirstAndLastName(String firstName, String lastName)	{
		return Optional.ofNullable(dao.findByFirstAndLastName(firstName, lastName));
	}

	public Optional<User> findByLoginOrEmail(String login, String email)	{
		return Optional.ofNullable(dao.findByLoginOrEmail(login, email));
	}

	public Optional<User> findByFirstAndLastNameWithMessages(String firstName, String lastName)	{
		Optional<User> user = Optional.ofNullable(dao.findByFirstAndLastName(firstName, lastName));
		if (user.isPresent())	{
			Hibernate.initialize(user.get().getMessagesReceived());
			Hibernate.initialize(user.get().getMessagesSent());
		}
		return user;
	}

	public Optional<User> findByLoginWithMsg(String receiver)	{
		Optional<User> user = Optional.ofNullable(dao.findByLogin(receiver));
		if (user.isPresent())	{
			Hibernate.initialize(user.get().getMessagesReceived());
			Hibernate.initialize(user.get().getMessagesSent());
		}
		return user;
	}

	public Optional<User> findByIdWithItemsAssigned(int id) {
		Optional<User> user =  Optional.ofNullable(dao.findUserById(id));
		if(user.isPresent())	{
			Hibernate.initialize(user.get().getItemsAssigned());
		}
		return user;
	}

	public Optional<User> findByIdWithItemsCreated(int id) {
		Optional<User> user =  Optional.ofNullable(dao.findUserById(id));
		if(user.isPresent())	{
			Hibernate.initialize(user.get().getItemsCreated());
		}
		return user;
	}

	public Optional<User> findByIdWithMessagesAndItemsAssigned(int id) {
		Optional<User> user =  Optional.ofNullable(dao.findUserById(id));
		if(user.isPresent())	{
			Hibernate.initialize(user.get().getMessagesReceived());
			Hibernate.initialize(user.get().getMessagesSent());
			Hibernate.initialize(user.get().getItemsAssigned());
		}
		return user;
	}

	public boolean simpleAuthentication(User user){
		return user.getId()!=0;
	}

	public boolean simpleAuthenticationWithIdStringOnly(String id) {return Integer.parseInt(id)!=0;}

	public boolean simpleAuthenticationWithAdminOrUser(User user){ return  user.getId()!=0;}

//	public void setUsersUniqueEmailConfirmationId(User user){
//		String confirmationId = UUID.randomUUID().toString();
//		user.setEmailConfirmationId(confirmationId);
//		updateUser(user);
//	}

	public void setUsersUniqueEmailConfirmationId(User user){
		String confirmationId = UUID.randomUUID().toString();
		user.setEmailConfirmationId(confirmationId);
	}

//	public void setUsersNextUniqueEmailConfirmationId(User user){
//		String confirmationId = UUID.randomUUID().toString();
//		System.out.println(confirmationId);
//		user.setEmailConfirmationId(confirmationId);
//	}

	public Set<Item> filterNewItems(Set<Item> allItems){
		Set<Item> newItems = new TreeSet<>();
		for (Item item : allItems){
			if (item.getItemState() == ItemState.NEW){
				newItems.add(item);
			}
		}
		return newItems;
	}

	public Set<Item> filterActiveItems(Set<Item> allItems){
		Set<Item> activeItems = new TreeSet<>();
		for (Item item : allItems){
			if (item.getItemState() == ItemState.ACTIVE){
				activeItems.add(item);
			}
		}
		return activeItems;
	}

	public Set<Item> filterResolvedItems(Set<Item> allItems){
		Set<Item> resolvedItems = new TreeSet<>();
		for (Item item : allItems){
			if (item.getItemState() == ItemState.RESOLVED){
				resolvedItems.add(item);
			}
		}
		return resolvedItems;
	}

	public Set<Item> filterClosedItems(Set<Item> allItems){
		Set<Item> closedItems = new TreeSet<>();
		for (Item item : allItems){
			if (item.getItemState() == ItemState.CLOSED){
				closedItems.add(item);
			}
		}
		return closedItems;
	}


	public Optional<User> findUserByEmailConfirmationId(String confirmationId) {
		return Optional.ofNullable(dao.findByEmailConfirmationId(confirmationId));
	}


	public Optional<User> findUserByEmail(String email) {
		return Optional.ofNullable(dao.findByEmail(email));
	}

	//	public Set<Message> getAllUserMessages(Integer id)	{
//		return findById(id).getAllUserMessages();
//	}

	public Optional<User> getUserById(Integer id) {
		return Optional.ofNullable(dao.findUserById(id));
	}

// USED PREVIOUSLY FOR PREAUTHORIZATION
//	@Override
//	public Set<Item> getUsersItemsAssigned(User user) {
//		return user.getItemsAssigned();
//	}
//
//	@Override
//	public Set<Message> getUsersMessagesReceived(User user) {
//		return user.getMessagesReceived();
//	}
//
//	@Override
//	public Set<Message> getUsersMessagesSent(User user) {
//		return user.getMessagesSent();
//	}

}
